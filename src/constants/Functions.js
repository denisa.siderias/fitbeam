import { useLayoutEffect, useState } from "react";

export const isEmpty = (data) => {
    if (data !== '' && data !== undefined)
        return false;

    return true;
}

export const addTime = (first_time, second_time) => {
    const first_time_array = (first_time ?? "").split(":");
    const second_time_array = (second_time ?? "").split(":");
    if (first_time_array.length > 2 && second_time_array.length > 2) {
        let hours = 0;
        let minutes = 0;
        let seconds = 0;

        hours = parseInt(first_time_array[0]) + parseInt(second_time_array[0]);
        minutes = parseInt(first_time_array[1]) + parseInt(second_time_array[1]);
        seconds = parseInt(first_time_array[2]) + parseInt(second_time_array[2]);

        if (seconds >= 60) {
            minutes = minutes + 1;
            seconds = seconds - 60;
        }

        if (minutes >= 60) {
            hours = hours + 1;
            minutes = minutes - 60;
        }
        return (hours < 10 ? "0" + hours : hours) + ":" + 
                (minutes < 10 ? "0" + minutes : minutes) + ":" + 
                (seconds < 10 ? "0" + seconds : seconds);
    }
    return "00:00:00";
}

export const useWindowSize = () => {
    const [size, setSize] = useState([0, 0]);
    useLayoutEffect(() => {
      function updateSize() {
        setSize([window.innerWidth, window.innerHeight]);
      }
      window.addEventListener('resize', updateSize);
      updateSize();
      return () => window.removeEventListener('resize', updateSize);
    }, []);
    return size;
  }