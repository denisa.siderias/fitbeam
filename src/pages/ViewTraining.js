import React, { useContext, useEffect, useRef, useState } from "react";
import { Image } from 'primereact/image';
import { deleteTrainingData, getTrainingData, getTrainingsData, setTrainingData } from "../services/TrainingOperations";
import { useParams } from "react-router-dom";
import { Rating } from "primereact/rating";
import { Button } from "primereact/button";
import { listenerChangeTrainingData } from "../services/ListenerData";
import { UserContext } from "../contexts/UserContext";
import { getUserData, setUserData } from "../services/UserOperations";
import { InputText } from "primereact/inputtext";
import { InputTextarea } from "primereact/inputtextarea";
import { Toast } from "primereact/toast";
import DropdownTemplate from "../components/DropdownTemplate";
import { getCategoriesData } from "../services/CategoryOperations";
import AddPhotoDialog from "../components/AddPhotoDialog";
import { RoutingContext } from "../contexts/RoutingContext";
import { TabView, TabPanel } from 'primereact/tabview';
import DataViewTemplate from "../components/DataViewTemplate";
import { Avatar } from "primereact/avatar";
import { isEmpty } from "@firebase/util";
import StartExercise from "../components/StartExercise";
import AddExerciseDialog from "../components/AddExerciseDialog";
import { Timestamp } from '@firebase/firestore';
import { addTime } from "../constants/Functions";

const ViewTraining = () => {
    const [trainings, setTrainings] = useState(null);
    const [trainer, setTrainer] = useState(null);
    const [isInFav, setIsInFav] = useState(false);
    const [editMode, setEditMode] = useState(false);
    const [thumbnail, setThumbnail] = useState("");
    const [trainingNameEdit, setTrainingNameEdit] = useState('');
    const [descriptionEdit, setDescriptionEdit] = useState('');
    const [ratingEdit, setRatingEdit] = useState(0);
    const [muscleCategoryEditOptions, setMuscleCategoryEditOptions] = useState([]);
    const [trainingCategoryEditOptions, setTrainingCategoryEditOptions] = useState([]);
    const [muscleCategoryEdit, setMuscleCategoryEdit] = useState(null);
    const [trainingCategoryEdit, setTrainingCategoryEdit] = useState(null);
    const [visibleAddPhoto, setVisibleAddPhoto] = useState(false);
    const [feedbackList, setFeedbackList] = useState([]);
    const [feedbackDescription, setFeedbackDescription] = useState('');
    const [feedbackRating, setFeedbackRating] = useState(0);
    const { history } = useContext(RoutingContext);
    const {training} = useParams();
    const toast = useRef(null);
    const [startExercise, setStartExercise] = useState(false);
    const [selectedExercise, setSelectedExercise] = useState(null);
    const {userData: {user, setUser}} = useContext(UserContext);
    const [visibleAddExercise, setVisibleAddExercise] = useState(false);

    const handleGetTrainingData = async (training) => {
        await getTrainingData("trainings", training).then(res => {
            if (res) {
                res.id = training;
                setTrainings({
                    ...res
                });
            } else {
                setTrainings(null);
            }

        })
    }

    const handleGetRatingsData = async (training) => {
        await getTrainingData("rating", training).then(res => {
            if (res) {
                let auxArray = [];
                res?.ratings?.forEach((element) => {
                    auxArray.push(element);
                })
                setFeedbackList(auxArray);
            }

        })
    }

    const handleGetFavData = async () => {
        if (user && trainings) {
            await getTrainingsData("favorites")
            .then(res => {
                const auxArray = []
                res?.forEach(element => {
                    if (element?.id === user?.firstName?.toLowerCase() + "-" + user?.lastName?.toLowerCase()) {
                        const data = element?.data();
                        data?.trainings?.map(item => {
                            if (item.id === trainings?.id)
                                auxArray.push({
                                    ...item
                                });
                        });
                    }  
                });
                
                if (auxArray.length > 0) {
                    setIsInFav(true);
                }
                
            });
        }
    }

    const handleGetTrainerData = async (id_trainer) => {
        await getUserData("users", id_trainer).then(res => {
            res.email = id_trainer;
            setTrainer({
                ...res
            });
        })
    }

    useEffect(() => {
        handleGetMuscleCategoriesData();
        handleGetTrainingCategoriesData();
    }, []);

    useEffect(() => {
        listenerChangeTrainingData("trainings", training)
        .then(res => {
           if (res) {
            res.id = training;
            setTrainings(res);
           }
        })
        .catch(error => {

        });

        listenerChangeTrainingData("rating", training)
        .then(res => {
           if (res) {
            let auxArray = [];
            res?.data()?.ratings?.forEach((element) => {
                auxArray.push(element);
            })
            setFeedbackList(auxArray);
           }
        })
        .catch(error => {

        });
    })

    useEffect(() => {
        if (training && !trainings) {
            handleGetTrainingData(training);
        }

        if (user && !isInFav) {
            handleGetFavData();
        }

        if (trainings && !trainer)
            handleGetTrainerData(trainings?.email_trainer)

        if (training) {
            handleGetRatingsData(training)
        }
    }, [trainings?.id, training]);

    useEffect(() => {
        if (editMode && trainings) {
            setThumbnail(trainings?.thumbnail ?? "");
            setTrainingNameEdit(trainings?.name ?? '');
            setDescriptionEdit(trainings?.description ?? '');
            setRatingEdit(trainings?.rating ?? 0);
            setMuscleCategoryEdit({code: trainings?.muscleCategory, name: muscleCategoryEditOptions.filter(element => element.code === trainings?.muscleCategory)?.[0]?.name} ?? '');
            setTrainingCategoryEdit({code: trainings?.trainingCategory, name: trainingCategoryEditOptions.filter(element => element.code === trainings?.trainingCategory)?.[0]?.name} ?? '');
        }
    }, [editMode]);


    const resetExitEditMode = () => {
        setEditMode(false);
        setTrainingNameEdit('');
        setDescriptionEdit('');
        setRatingEdit(0);
        setThumbnail("");
    }

    const handleGetMuscleCategoriesData = async () => {
        if (user) {
            await getCategoriesData("muscle-category").then(res => {
              const auxArray = []
              res?.forEach(element => {
                  auxArray.push({
                    code: element.id,
                    name: element.data().name
                  })
              });
              setMuscleCategoryEditOptions(auxArray);
          })
        }
    }

    const handleGetTrainingCategoriesData = async () => {
        if (user) {
            await getCategoriesData("training-category").then(res => {
              const auxArray = []
              res?.forEach(element => {
                  auxArray.push({
                    code: element.id,
                    name: element.data().name
                  })
              });
              setTrainingCategoryEditOptions(auxArray);
          })
        }
    }

    const showSuccess = () => {
        if (toast)
            toast.current.show({severity:'success', summary: 'Operation succed', detail:'Data has been modified', life: 3000});
    };

    const showWarn = (message) => {
        if (toast)
            toast.current.show({severity:'warn', summary: 'Warning', detail: message, life: 3000});
    };

    const showError = () => {
        if (toast)
            toast.current.show({severity:'error', summary: 'Operation failed', detail:'Data hasn\'t been modified', life: 3000});
    }

    const handleSaveData = async () => {
        if (trainingNameEdit !== '' && descriptionEdit !== '' && thumbnail !== '' && 
            user && trainings && muscleCategoryEdit && trainingCategoryEdit
            && training) {
            await setTrainingData("trainings", trainings.id, {
                muscleCategory: muscleCategoryEdit.code,
                trainingCategory: trainingCategoryEdit.code,
                description: descriptionEdit,
                email_trainer: user?.email,
                name: trainingNameEdit,
                trainer: user?.firstName + ' ' + user?.lastName,
                thumbnail: thumbnail,
                rating: ratingEdit
            }, { merge: true }).then(res => {
                if (res) {
                  res.id = training;
                  setTrainings(res);
                  resetExitEditMode();
                    showSuccess();
                } else { 
                    showError();
                }
            })
        }
    }

    const handleRemovePhoto = async () => {
        setThumbnail("");
    };

    const handleUploadPhoto = async (file) => {
        if (file) {
            setThumbnail(file);
        }
        setVisibleAddPhoto(false);
    };

    const handleAddToFavorites = async () => {
        if (user && trainings) {
            await getTrainingsData("favorites")
            .then(async res => {
                trainings.muscleCategoryName = muscleCategoryEditOptions.filter(element => element.code === trainings?.muscleCategory)?.[0]?.name;
                trainings.trainingCategoryName = trainingCategoryEditOptions.filter(element => element.code === trainings?.trainingCategory)?.[0]?.name
                const auxArray = []
                res?.forEach(element => {
                    if (element?.id === user?.firstName?.toLowerCase() + "-" + user?.lastName?.toLowerCase()) {
                        const data = element?.data();
                        data?.trainings?.map(item => {
                            auxArray.push({
                                ...item
                            });
                        })
                    }  
                });
                
                await setTrainingData("favorites", user?.firstName?.toLowerCase() + '-' + user?.lastName?.toLowerCase(), {
                    trainings: [...auxArray, trainings]
                }, { merge: true }).then(res => {
                    if (res) {
                        showSuccess();
                        setIsInFav(true);
                    }
                })
                .catch(err => {
                    showError();
                })
            });
        }

    }

    const handleTrainerChat = async () => {
        if (training && user) {
            await getUserData("messages", `${trainings?.email_trainer}_${user?.email}`)
            .then(async res => {
                if (!res?.hasOwnProperty("customData")) {
                    if (!res) {
                        await setUserData("messages", `${trainings?.email_trainer}_${user?.email}`, {
                            messages: []
                        }).then(res => {
                            if (!res?.hasOwnProperty("customData")) {
                                history(`/chatmessages/${trainings?.email_trainer}_${user?.email}`);
                            }
                        })
                    } else
                        history(`/chatmessages/${trainings?.email_trainer}_${user?.email}`);
                }
            })
        }
    }

    const handleAddRating = async () => {
        if (training && user) {
            await setTrainingData("rating", training, {
                ratings: [...feedbackList, {
                    description: feedbackDescription,
                    stars: feedbackRating,
                    name: user?.firstName + ' ' + user?.lastName,
                    email_user: user?.email
                }]
            }, { merge: true }).then(async res => {
                if (res) {
                    await setTrainingData("trainings", training, {
                        ...trainings,
                        rating: feedbackList?.length > 0 ?
                        (feedbackList
                        .map(element => element.stars)
                        .reduce((accumulator, currentValue) => accumulator + currentValue, 0) + +feedbackRating) / 
                        (feedbackList?.length + 1) : feedbackRating ?? 0
                    }, { merge: true });
                    showSuccess();
                    setFeedbackList([...feedbackList, {
                        description: feedbackDescription,
                        stars: feedbackRating,
                        name: user?.firstName + ' ' + user?.lastName,
                        email_user: user?.email
                    }]);
                    setFeedbackDescription('');
                    setFeedbackRating(0);
                }
            })
            .catch(err => {
                showError();
            })
        }
    }

    const handleDeleteFeedbackTraining = async (id_rating) => {
        if (id_rating && training) {
            await setTrainingData("rating", training, {
                ratings: feedbackList?.filter(element => element?.email_user !== id_rating)
            }, { merge: true }).then(async res => {
                if (res) {
                    await setTrainingData("trainings", training, {
                        ...trainings,
                        rating: feedbackList?.length > 0 ?
                        feedbackList.filter(element => element?.email_user !== id_rating)
                        .map(element => element.stars)
                        .reduce((accumulator, currentValue) => accumulator + currentValue, 0) ?? 0 / 
                        feedbackList?.filter(element => element?.email_user !== id_rating).length ?? 1 : 0
                    }, { merge: true });
                    showSuccess();
                    setFeedbackList(feedbackList?.filter(element => element?.email_user !== id_rating));
                }
            })
            .catch(err => {
                showError();
            })
        }
    };

    const handleDeleteTraining = async () => {
        if (training) {
            await deleteTrainingData("trainings", training)
            .then(async res => {
                if (res === undefined) {
                    await getTrainingsData("favorites").then(res => {
                        res?.forEach(async element => {
                            const data = element?.data()?.trainings;
                            const findTraining = data?.filter(item => item?.id === training);
                            if (findTraining.length > 0) {
                                await setTrainingData("favorites", user?.firstName?.toLowerCase() + '-' + user?.lastName?.toLowerCase(), {
                                    trainings: data?.filter(item => item?.id !== training)
                                }, { merge: true })
                            }
                        });
                    })
                    await deleteTrainingData("rating", training);
                    showSuccess();
                    setTimeout(() => {
                        history('/home');
                    }, 1500)
                } else {
                    showWarn("Can't find the trainings")
                }
            })
            .catch(error => {
                showError();
            });
        }
    }

    const handleDeleteExerciseTraining = async (id_exercise) => {
        if (id_exercise && trainings?.exercises && training) {
            await setTrainingData("trainings", trainings.id, {
                ...trainings,
                exercises: trainings.exercises.filter((element) => element.id_exercise !== id_exercise)
            }, { merge: true }).then(res => {
                if (res) {
                  res.id = training;
                  setTrainings(res);
                    showSuccess();
                } else { 
                    showError();
                }
            })
        }
    };

    const handleStartExercise = (exercise) => {
        setStartExercise(true);
        setSelectedExercise(exercise);
    };

    const handleEndExercise = async (seconds, minutes, hours) => {
        if (selectedExercise && user) {
            const hoursAux = hours < 10 ? "0" + hours : hours;
            const minuteAux = minutes < 10 ? "0" + minutes : minutes;
            const secondsAux = seconds < 10 ? "0" + seconds : seconds;
            await setUserData("users", user.email, {exercises: [...user?.exercises, {
                ...selectedExercise,
                time: hoursAux + ":" + minuteAux + ":" + secondsAux,
                caloriesConsumed: parseFloat(((+(hours * 3600) + +(minutes * 60) + +seconds) / 60) * selectedExercise.calories),
                date: Timestamp.now()
            }]}, { merge: true })
            .then(res => {
                if (res) {  
                    setUser(res);
                    showSuccess();
                }
            })
        }
    };

    return (
        <div className="grid p-0 m-0">
            <div className="col-12 md:col-12 lg:col-12">
                <Toast ref={toast} />
            </div>
            <TabView className="w-full">
                <TabPanel header="General Info" className="align-center">
                    <div className="grid w-8 align-center">
                            <div className="col-12 md:col-12 lg:col-12 flex align-items-center justify-content-center">
                                {
                                    editMode ?
                                        <div className="col-1" style={{position: 'relative', width: 'auto'}}>
                                            {thumbnail !== "" && <Button icon="pi pi-times" className="icon-btn" rounded="true" outlined="true" severity="danger" aria-label="Cancel" 
                                            style={{position: 'absolute', top: '-5%', left: '100%', transform: 'translate(-70%, 30%)', zIndex: '1'}} 
                                            onClick={() => {
                                                handleRemovePhoto()
                                            }}/>}
                                            <Image 
                                                src={thumbnail}
                                                alt="Image"
                                                width="500"
                                                style={{ cursor: 'pointer', width: '300px', height: '250px' }} 
                                            />
                                        </div>
                                    :
                                        <Image src={trainings?.thumbnail ?? ""} alt="Image" width="500" preview />
                                }
                            </div>
                            {
                                user?.type === 'trainer' && editMode && thumbnail === "" ?
                                <div className="col-12 md:col-8 lg:col-8">
                                    <Button label={'Add photo'} className="icon-btn" onClick={() => setVisibleAddPhoto(true)}/>
                                    <AddPhotoDialog
                                        visibleDialog={visibleAddPhoto}
                                        setVisibleDialog={setVisibleAddPhoto}
                                        onUpload={(file) => handleUploadPhoto(file)}
                                    />
                                </div>
                                :
                                null
                            }
                            <div className="col-8 grid">
                                <div className="col-12 md:col-6 lg:col-6 align-left" style={{ justifyContent: 'flex-start' }}>
                                    <h2>Training name</h2>
                                    {
                                        editMode ?
                                            <InputText
                                                placeholder="Type training name" 
                                                value={trainingNameEdit} 
                                                onChange={(e) => setTrainingNameEdit(e.target.value)}
                                            />
                                        :
                                            <p>{trainings?.name ?? 'No info'}</p>
                                    }        
                                </div>
                                <div className="col-12 md:col-6 lg:col-6 align-center">
                                    {
                                        user?.type === 'trainer' ?
                                            editMode ?
                                                <Button label={'Exit edit mode'} className="icon-btn" onClick={() => resetExitEditMode()}/>
                                            :
                                                <Button label={'Edit mode'} className="icon-btn" onClick={() => setEditMode(true)}/>
                                        :
                                            ""       
                                    }
                                    {
                                            user?.type === 'trainer' && 
                                                <Button label={'Delete Training'} className="icon-btn my-3" onClick={() => handleDeleteTraining()}/>
                                    }
                                    <h3>Rating</h3>
                                    <Rating value={trainings?.rating ?? 0} readOnly={true} cancel={false} />

                                    {user?.type === 'champion' && <Button label="Add to Favorites" className="icon-btn mt-4" disabled={isInFav} onClick={() => handleAddToFavorites()}/>}
                                    {user?.type === 'champion' && <Button label="Chat with trainer" className="icon-btn mt-4" onClick={() => handleTrainerChat()}/>}
                                </div>
                                <div className="col-12 md:col-6 lg:col-6 align-left" style={{ justifyContent: 'flex-start'}}>
                                    <h4>Your Trainer</h4>
                                    <p>{trainings?.trainer ?? 'No info'}</p>
                                </div>
                                <div className="col-12 md:col-6 lg:col-6 align-center">
                                    <div className="align-left">
                                        <h4>Muscle Category</h4>
                                        {
                                            editMode ?
                                                <DropdownTemplate
                                                    value={muscleCategoryEdit} 
                                                    onChange={(e) => setMuscleCategoryEdit(e.value)} 
                                                    options={muscleCategoryEditOptions} 
                                                    optionLabel="name" 
                                                    placeholder="Select a option" 
                                                    filter
                                                    showClear
                                                    className="w-full md:w-14rem" 
                                                />
                                            :
                                                <p>{muscleCategoryEditOptions.filter(element => element.code === trainings?.muscleCategory)?.[0]?.name ?? 'No info'}</p>
                                        }
                                    </div>
                                    
                                </div>
                                <div className="col-12 md:col-6 lg:col-6 align-left">
                                    <h5>Training Category</h5>
                                    {
                                        editMode ?
                                            <DropdownTemplate
                                                value={trainingCategoryEdit} 
                                                onChange={(e) => setTrainingCategoryEdit(e.value)} 
                                                options={trainingCategoryEditOptions} 
                                                optionLabel="name" 
                                                placeholder="Select a option" 
                                                filter
                                                showClear 
                                            />
                                        :
                                            <p>{trainingCategoryEditOptions.filter(element => element.code === trainings?.trainingCategory)?.[0]?.name ?? 'No info'}</p>
                                    }
                                </div>
                                <div className="col-12 md:col-12 lg:col-12">
                                    <h5>Description</h5>
                                    {
                                        editMode ?
                                            <InputTextarea
                                                value={descriptionEdit} 
                                                onChange={(e) => setDescriptionEdit(e.target.value)} 
                                                rows={5} cols={30}
                                                className="description"
                                            />
                                        :
                                            <p className="description">{trainings?.description ?? 'No info'}</p>
                                    }
                                    
                                </div>
                            </div>
                            {
                                editMode &&
                                <div className="col-12 md:col-8 lg:col-8">
                                    <Button label='Cancel' className="icon-btn sm:mr-0 md:mr-2 lg:mr-2" outlined="true" onClick={() => resetExitEditMode()}/>
                                    <Button label='Save' className="icon-btn" onClick={() => handleSaveData()}/>
                                </div>
                            }
                    </div>

                </TabPanel>
                <TabPanel header="Exercises">
                    {user?.type === "trainer" && <Button label="Add exercise" className="icon-btn" onClick={() => setVisibleAddExercise(true)}/>}
                    {user?.type === "trainer" && 
                    <AddExerciseDialog
                        visibleDialog={visibleAddExercise}
                        setVisibleDialog={setVisibleAddExercise}
                        trainingId={training}
                        exercises={trainings?.exercises ?? []}
                        onSuccess={(data) => {
                            if (data && training) {
                                handleGetTrainingData(training);
                                showSuccess();
                            }
                        }}
                    />}
                    <DataViewTemplate
                        data={trainings?.exercises ?? []}
                        layout="list"
                        renderListItem={(data) => (<div className="col-12">
                            <div className="flex flex-column align-items-center p-3 w-full md:flex-row">
                                {/* <Avatar label={data?.name?.[0]?.toUpperCase() ?? "U"} size="large" shape="circle" /> */}
                                <Image src={data?.gif} alt="Image" width="250" preview />
                            {/* <img className="md:w-11rem w-9 shadow-2 md:my-0 md:mr-5 mr-0 my-5 w-full h-7rem" src={data?.image ?? ''}  alt={data?.name} /> */}
                                <div className="text-center md:text-left md:flex-1 ml-3">
                                    <div className="text-2xl font-bold">{data?.name}</div>
                                    <div className="text-lg">{data?.description?.substring(0, 100)}</div>
                                    {user?.type === "champion" ? 
                                        <div className="text-md">{user?.exercises
                                            .filter((element) => element.id_exercise === data?.id_exercise)
                                            .map(element => element.time)
                                            .reduce((accumulator, currentValue) => {
                                                return addTime(accumulator, currentValue)
                                            }, "00:00:00") ?? "00:00:00"}</div>
                                    :
                                        <div className="text-md">{data?.calories} calories/minute</div>}
                                </div>
                                {
                                    user?.type === "trainer" ?
                                        <div className="flex md:flex-column mt-5 justify-content-between align-items-center md:w-auto w-full">
                                            <Button className="icon-btn mb-2" icon="pi pi-times"  label="Remove Exercise" onClick={() => handleDeleteExerciseTraining(data?.id_exercise)}></Button>
                                        </div>
                                    :
                                        <div className="flex md:flex-column mt-5 justify-content-between align-items-center md:w-auto w-full">
                                            <Button
                                                className="icon-btn mb-2"
                                                icon={`pi ${user?.type === "champion" ? "pi-stopwatch" : "pi-eye"}`}
                                                label={`${user?.type === "champion" ? "Start" : "View"} exercise`}
                                                onClick={() => handleStartExercise(data)}
                                            />
                                        </div>
                                }
                                
                            </div>
                        </div>)}
                        rows={9}
                    />
                </TabPanel>
                <TabPanel header="Ratings">
                    <DataViewTemplate
                        data={feedbackList}
                        layout="list"
                        renderListItem={(data) => (<div className="col-12">
                            <div className="flex flex-column align-items-center p-3 w-full md:flex-row">
                                <Avatar label={data?.name?.[0]?.toUpperCase() ?? "U"} size="large" shape="circle" />
                                {/* <Image src={data?.userProfile} alt="Image" width="250" /> */}
                            {/* <img className="md:w-11rem w-9 shadow-2 md:my-0 md:mr-5 mr-0 my-5 w-full h-7rem" src={data?.image ?? ''}  alt={data?.name} /> */}
                                <div className="text-center md:text-left md:flex-1 ml-3">
                                    <Rating className="mb-2" value={data?.stars} readOnly cancel={false}></Rating>
                                    <div className="text-2xl font-bold">{data?.name}</div>
                                    <div className="text-lg description">{data?.description}</div>
                                </div>
                                {
                                    user?.type === "trainer" &&
                                    <div className="flex md:flex-column mt-5 justify-content-between align-items-center md:w-auto w-full">
                                        <Button className="icon-btn mb-2" icon="pi pi-times" label="Remove Feedback" onClick={() => handleDeleteFeedbackTraining(data?.email_user)}></Button>
                                    </div>
                                }
                                
                            </div>
                        </div>)}
                        rows={9}
                    />
                    {
                        user?.type === "champion" &&
                        <div className="flex flex-column">
                            <h3>Rating</h3>
                            <Rating 
                                value={feedbackRating}
                                readOnly={feedbackList?.filter(element => element.email_user === user?.email).length > 0} 
                                cancel={true}
                                onChange={(e) => setFeedbackRating(e.value)}
                            />
                            <InputTextarea
                                className="mt-2"
                                value={feedbackDescription}
                                readOnly={feedbackList?.filter(element => element.email_user === user?.email).length > 0}
                                onChange={(e) => setFeedbackDescription(e.target.value)} 
                                rows={5} cols={30} 
                            />
                            <Button 
                                label="Add Feedback"
                                className="icon-btn w-2 mt-2" 
                                disabled={
                                    isEmpty(feedbackDescription) || feedbackRating === 0 ||
                                feedbackList?.filter(element => element.email_user === user?.email).length > 0} 
                                onClick={() => handleAddRating()} 
                            />
                        </div>
                    }

                </TabPanel>
            </TabView>
            {
                user?.type !== "trainer" && 
                <StartExercise
                    open={startExercise}
                    name={selectedExercise?.name ?? ""}
                    description={selectedExercise?.description ?? ""}
                    image={selectedExercise?.gif ?? ""}
                    timeSpent={(user?.exercises ?? [])
                        .filter((element) => element.id_exercise === selectedExercise?.id_exercise)
                        .map(element => element.time)
                        .reduce((accumulator, currentValue) => {
                            return addTime(accumulator, currentValue)
                        }, "00:00:00") ?? "00:00:00"}
                    time={"00:00:00"}
                    onHide={() => setStartExercise(false)}
                    endExercise={(seconds, minutes, hours) => handleEndExercise(seconds, minutes, hours)}
                />
            }

        </div>
    )
}

export default ViewTraining;