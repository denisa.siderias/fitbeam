import { getUserData, setUserData } from '../services/UserOperations';
import { Password } from 'primereact/password';
import '../styles/Login.css';
import { InputText } from 'primereact/inputtext';
import { useContext, useRef, useState } from 'react';
import { Button } from 'primereact/button';
import { RoutingContext } from '../contexts/RoutingContext';
import { UserContext } from '../contexts/UserContext';
import { Link } from 'react-router-dom';
import { Toast } from 'primereact/toast';
import { getAuth, signInWithEmailAndPassword } from 'firebase/auth';

function Login() {

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const { history } = useContext(RoutingContext);
  const { userData: {setUser} } = useContext(UserContext);
  const auth = getAuth();

  const toast = useRef(null);

  const showWarn = () => {
    toast.current.clear();

    toast.current.show({
        severity:'warn', 
        summary: 'Account doesn\'t exists', 
        detail:'Doesn\'t exist an account with this email, try to register', 
        life: 3000
    });
  }

  const showError = () => {
    toast.current.clear();
    
    toast.current.show({
        severity:'error', 
        summary: 'Login failed', 
        detail:'The password isn\'t correct', 
        life: 3000
    });
  }

  const handleLogin = () => {
    if (email && password) {
        signInWithEmailAndPassword(auth, email, password)
        .then(async (userCredential) => {
            await setUserData("users", email, {
                isLogged: true
            }, { merge: true }).then(async res => {
                if (res) {
                    // Signed in 
                    getUserData("users", userCredential.user.email).then(res => {
                        res.email = email;
                        setUser({
                            ...res
                        });
                        if (res?.firstLogin) {
                            history('/profile');
                        } else {
                            history('/home');
                        }
                    })
                }
            })
        })
        .catch((error) => {
            if (error.code === "auth/user-not-found")
                showWarn();
            else if (error.code === "auth/wrong-password")
                showError();
        });
    }
  }

  return (
    <div className="Login align-center">
        <div className='form-data align-center shadow-6'>
            <div className='align-left w-full relative'>
                <h3>Email</h3>
                <span className="p-input-icon-right relative w-full">
                    <i className="pi pi-envelope" />
                    <InputText
                        value={email} 
                        onChange={(e) => setEmail(e.target.value)}
                        className="w-full"
                        placeholder="Email" 
                    />
                </span>
            </div>
            <div className='align-left margin-bottom-between w-full relative'>
                <h3>Password</h3>
                <Password 
                    value={password} 
                    placeholder='Password'
                    className='relative w-full password-field'
                    onChange={(e) => setPassword(e.target.value)} 
                    feedback={false}
                    toggleMask />
            </div>
            <div className='align-center margin-bottom-between'>
                <Button label="Log In" className="p-button-raised p-button-info bg-white text-blue-400" onClick={() => handleLogin()}/>
            </div>
            <div className='align-center margin-bottom-between'>
                <p className='margin-zero'>Not a member?</p>
                <Link to='/register' className="text-white no-underline mt-3">Sign up now</Link>
            </div>
        </div>
        <Toast ref={toast} />
    </div>
  );
}

export default Login;
