import { Button } from 'primereact/button';
import React, { useContext, useEffect, useRef, useState } from 'react';
import { useParams } from 'react-router-dom';
import ChatMessage from '../components/ChatMessage';
import { UserContext } from '../contexts/UserContext';
import { listenerChangeTrainingData } from '../services/ListenerData';
import { Timestamp } from '@firebase/firestore';
import { Avatar } from 'primereact/avatar';
import '../styles/ChatMessages.css';
import { getUserData, setUserData } from '../services/UserOperations';
import moment from 'moment';
import { Badge } from 'primereact/badge';
import VirtualScrollList from '../components/VirtualScrollList';
import { getMessages } from '../services/ChatMessages';
import { useWindowSize } from '../constants/Functions';
import { Message } from 'primereact/message';

const ChatMessages = () => {
    const {nameConversation} = useParams();
    const [chatMessages, setChatMessages] = useState([]);
    const [chat, setChat] = useState([]);
    const [messageChat, setMessageChat] = useState('');
    const { userData: {user} } = useContext(UserContext);
    const [width, height] = useWindowSize();
    const [activePeriod, setActivePeriod] = useState({
        time: 1,
        unit: 'second'
    });
    const [trainer, setTrainer] = useState(null);
    const dummyRef = useRef(null);

    const handleGetTrainerMessagesList = async () => {
        await getMessages("messages")
        .then(res => {
            if (!res?.hasOwnProperty("customData")) {
                res?.forEach(element => {
                    if (element.id.split("_")[0] === user?.email) {
                        setChatMessages([...chatMessages, { id: element.id }]);
                    }
                })
            }
        })
    };

    const handleGetChampionMessagesList = async () => {
        await getMessages("messages")
        .then(res => {
            if (!res?.hasOwnProperty("customData")) {
                res?.forEach(element => {
                    if (element.id.split("_")[1] === user?.email) {
                        setChatMessages([...chatMessages, { id: element.id }]);
                    }
                })
            }
        })
    };

    const handleGetTrainerData = async () => {
        if (nameConversation && user) {
            await getUserData("users", user?.type === "trainer" ? nameConversation.split("_")[1] : nameConversation.split("_")[0]).then(res => {
                if (res) {
                    let loggedDate = new Date(res?.lastLogged.seconds * 1000 + res?.lastLogged.nanoseconds / 1000000);
                    const startDate = moment(loggedDate);
                    const endDate = moment(new Date());
                    const diff = endDate.diff(startDate);
                    const diffDuration = moment.duration(diff);
                    let period = 1;
                    let unitPeriod = 'second'
                    if (diffDuration.seconds() > 1) {
                        period = diffDuration.seconds();
                        unitPeriod = 'seconds';
                    }

                    if (diffDuration.minutes() > 0) {
                        period = diffDuration.minutes();
                        if (diffDuration.minutes() === 1)
                            unitPeriod = 'minute';
                        else
                            unitPeriod = 'minutes';
                    }

                    if (diffDuration.hours() > 0) {
                        period = diffDuration.hours();
                        if (diffDuration.hours() === 1)
                            unitPeriod = 'hour';
                        else
                            unitPeriod = 'hours';
                    }

                    if (diffDuration.days() > 0) {
                        period = diffDuration.days();
                        if (diffDuration.days() === 1)
                            unitPeriod = 'day';
                        else
                            unitPeriod = 'days';
                    }

                    setActivePeriod({
                        time: period,
                        unit: unitPeriod
                    });

                    res.email = nameConversation.split("_")[0];
                    setTrainer({
                        ...res
                    });
                } else {
                    setTrainer(null);
                }
    
            })
        }
    }

    const handleGetMessages = async () => {
        await getUserData("messages", nameConversation)
                .then(res => {
                    if (res?.hasOwnProperty("messages")) {
                        if (res?.messages?.filter(element => !chat?.map(msg => msg?.text).includes(element?.text)).length > 0) {
                            setChat(res?.messages);
                            if (dummyRef) {
                                dummyRef.current.scrollIntoView({ behavior: 'smooth' });
                            }
                        }
                    }
                })
    }

    useEffect(() => {
        if (user?.type === "trainer" && user?.checkedAccount) {
            handleGetTrainerMessagesList();
        } else if (user?.type === "champion") {
            handleGetChampionMessagesList();
        }

    }, []);

    useEffect(() => {
        if (nameConversation && chatMessages?.length === 0) {
            handleGetMessages();
        }

        if (!trainer && nameConversation) {
            handleGetTrainerData();
        }
    }, [nameConversation]);

    useEffect(() => {
        if (nameConversation && ((user?.checkedAccount && user?.type === "trainer") || user?.type !== "trainer")) {
            listenerChangeTrainingData("messages", nameConversation)
            .then(res => {
            if (res) {
                        handleGetMessages();
                        if (user?.type === "trainer") {
                            handleGetTrainerMessagesList();
                        } else {
                            handleGetChampionMessagesList();
                        }
            }
            });
        }

    });

    useEffect(() => {
        listenerChangeTrainingData("users", user?.email)
        .then(res => {
        if (res) {
                if (res?.data()?.checkedAccount) {
                handleGetMessages();
                if (user?.type === "trainer") {
                    handleGetTrainerMessagesList();
                } else {
                    handleGetChampionMessagesList();
                }
            }
        }
        });
    }, [])

    const sendMessage = async (e) => {
        e.preventDefault();
        if (user && trainer && nameConversation && messageChat) {
            await setUserData('messages', nameConversation, {
                messages: [...chat, {
                    text: messageChat,
                    sender: user?.email,
                    receiver: trainer?.email,
                    createdAt: Timestamp.now(),
                }]
            }, { merge: true })
            .then(res => {
                if (res?.hasOwnProperty("messages")) {
                    setMessageChat("");
                    setChat(res?.messages ?? []);
                    if (dummyRef) {
                        dummyRef.current.scrollIntoView(false, { behavior: 'smooth' });
                    }
                }
            })
        }
    }

    if (width < 768)
        if (chat && nameConversation)
            return (<div className='pb-4'>
                <div className='flex align-items-center justify-content-start p-2' style={{boxShadow: '0 5px 12px 0px #dadada'}}>
                    <Avatar 
                        className='p-overlay-badge'
                        label={trainer?.firstName?.[0]?.toUpperCase() ?? ''} 
                        size="large" 
                        style={{ backgroundColor: '#dadada', color: '#ffffff' }} 
                        shape="circle" 
                    >
                        {trainer?.isLogged && <Badge value="" severity='success' style={{top: '90%', right: '25%', width: '16px', height: '16px', border: '3px solid #fff'}} />}
                    </Avatar>
                    <div className='ml-2 p-0'>
                        <h4 className='my-0'>{(trainer?.firstName ?? "") + " " + (trainer?.lastName ?? "") ?? ''}</h4>
                        <h5 className='my-0 font-normal'>{trainer?.isLogged ? 'Active now' : `Active ${activePeriod?.time + ' ' + activePeriod.unit} ago`}</h5>
                    </div>
                </div>
                <div className='overflow-y-auto p-2 chat-body'>
                    {chat?.sort((firstComment, secondComment) => {
                        if (firstComment?.createdAt > secondComment?.createdAt)
                            return 1;
                        
                        if (firstComment?.createdAt < secondComment?.createdAt)
                            return -1;
                        
                        return 0;
                    })
                    ?.map((msg, index) => <ChatMessage key={`message-${index}`} message={msg} />)}
                    <div ref={dummyRef}></div>
                </div>
                <form onSubmit={sendMessage} className="w-full flex pr-2">
                    <input 
                        value={messageChat} 
                        onChange={(e) => setMessageChat(e.target.value)} 
                        className="flex-grow-1 surface-200 input-form-chat"
                        placeholder='Aa'
                    />
                    <Button type="submit" label='Sent' className="icon-btn" />
                </form>
            </div>)
        else
            return (
            <div className='align-center h-full'>
                {!user?.checkedAccount && user?.type === "trainer" && <Message severity="error" className='my-4' text="Your account isn't checked, please wait until the admin confirm your account" />}
                <VirtualScrollList
                    items={(chatMessages ?? []).sort((firstComment, secondComment) => {
                        if (new Date(firstComment?.createdAt?.seconds * 1000 + firstComment?.createdAt?.nanoseconds / 1000000)
                            < 
                        new Date(secondComment?.createdAt?.seconds * 1000 + secondComment?.createdAt?.nanoseconds / 1000000)
                        )
                            return 1;
                        
                        if (new Date(firstComment?.createdAt?.seconds * 1000 + firstComment?.createdAt?.nanoseconds / 1000000)
                            > 
                            new Date(secondComment?.createdAt?.seconds * 1000 + secondComment?.createdAt?.nanoseconds / 1000000))
                            return -1;
                        
                        return 0;
                    }).filter((message, index) => {
                        return index === chatMessages
                        ?.findIndex(obj => obj.id === message.id)
                    })
                    ?.map(element => 
                        ({ id: element?.id,
                        label: user?.type === "trainer" ? element?.id?.split("_")[1][0].toUpperCase() : element?.id[0][0].toUpperCase(),
                        name: user?.type === "trainer" ? element?.id?.split("_")[1] : element?.id?.split("_")[0]}))}
                    itemSize={50}
                    clicked={nameConversation}
                    title="Conversations"
                />
            </div>
            )

    return (
        <div className="grid m-0">
            {
                !user?.checkedAccount && user?.type === "trainer" &&
                <div className='col-12 align-center'>
                    <Message severity="error" className='my-4' text="Your account isn't checked, please wait until the admin confirm your account" />
                </div>
            }
            <div className='col-12 md:col-4 lg:col-3 pr-0 h-full'>
                <VirtualScrollList
                    items={(chatMessages ?? []).sort((firstComment, secondComment) => {
                        if (new Date(firstComment?.createdAt?.seconds * 1000 + firstComment?.createdAt?.nanoseconds / 1000000)
                            < 
                        new Date(secondComment?.createdAt?.seconds * 1000 + secondComment?.createdAt?.nanoseconds / 1000000)
                        )
                            return 1;
                        
                        if (new Date(firstComment?.createdAt?.seconds * 1000 + firstComment?.createdAt?.nanoseconds / 1000000)
                            > 
                            new Date(secondComment?.createdAt?.seconds * 1000 + secondComment?.createdAt?.nanoseconds / 1000000))
                            return -1;
                        
                        return 0;
                    }).filter((message, index) => {
                        return index === chatMessages
                        ?.findIndex(obj => obj.id === message.id)
                    })
                    ?.map(element => 
                        ({ id: element?.id,
                        label: user?.type === "trainer" ? element?.id?.split("_")[1][0].toUpperCase() : element?.id[0][0].toUpperCase(),
                        name: user?.type === "trainer" ? element?.id?.split("_")[1] : element?.id?.split("_")[0]}))}
                    itemSize={50}
                    clicked={nameConversation}
                    title="Conversations"
                />
            </div>
            {
                chat && nameConversation &&
                <div className='col-12 md:col-8 lg:col-9 pl-0 h-full'>
                    <div className='flex align-items-center justify-content-start p-2' style={{boxShadow: '0 5px 12px 0px #dadada'}}>
                        <Avatar 
                            className='p-overlay-badge'
                            label={trainer?.firstName?.[0]?.toUpperCase() ?? ''} 
                            size="large" 
                            style={{ backgroundColor: '#dadada', color: '#ffffff' }} 
                            shape="circle" 
                        >
                            {trainer?.isLogged && <Badge value="" severity='success' style={{top: '90%', right: '25%', width: '16px', height: '16px', border: '3px solid #fff'}} />}
                        </Avatar>
                        <div className='ml-2 p-0'>
                            <h4 className='my-0'>{(trainer?.firstName ?? "") + " " + (trainer?.lastName ?? "") ?? ''}</h4>
                            <h5 className='my-0 font-normal'>{trainer?.isLogged ? 'Active now' : `Active ${activePeriod?.time + ' ' + activePeriod.unit} ago`}</h5>
                        </div>
                    </div>
                    <div className='overflow-y-auto p-2 m-2 chat-body'>
                        {chat?.sort((firstComment, secondComment) => {
                            if (firstComment?.createdAt > secondComment?.createdAt)
                                return 1;
                            
                            if (firstComment?.createdAt < secondComment?.createdAt)
                                return -1;
                            
                            return 0;
                        })
                        ?.map((msg, index) => <ChatMessage key={`message-${index}`} message={msg} />)}
                        <div ref={dummyRef}></div>
                    </div>
                    <form onSubmit={sendMessage} className="w-full flex pr-2">
                        <input 
                            value={messageChat} 
                            onChange={(e) => setMessageChat(e.target.value)} 
                            className="flex-grow-1 surface-200 input-form-chat"
                            placeholder='Aa'
                        />
                        <Button type="submit" label='Sent' className="icon-btn" />
                    </form>
                </div>
            }
        </div>
    );
};

export default ChatMessages;