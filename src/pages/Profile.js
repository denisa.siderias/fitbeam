import { Button } from 'primereact/button';
import { InputText } from 'primereact/inputtext';
import { useContext, useEffect, useRef, useState } from 'react';
import { RoutingContext } from '../contexts/RoutingContext';
import { UserContext } from '../contexts/UserContext';
import { setUserData } from '../services/UserOperations';
import '../styles/Profile.css';
import { Message } from 'primereact/message';
import { Image } from 'primereact/image';
import AddPhotoDialog from '../components/AddPhotoDialog';
import { isEmpty } from '../constants/Functions';
import { Toast } from 'primereact/toast';
import DropdownTemplate from '../components/DropdownTemplate';

function Profile() {
 const toast = useRef(null);
 const { userData: {user, setUser} } = useContext(UserContext);
 const { history } = useContext(RoutingContext);
 const [editMode, setEditMode] = useState(false);
 const [email, setEmail] = useState('');
 const [firstName, setFirstName] = useState('');
 const [lastName, setLastName] = useState('');
 const [visibleAddPhoto, setVisibleAddPhoto] = useState(false);
 const [diploma, setDiploma] = useState(null);
 const [height, setHeight] = useState(0);
 const [weight, setWeight] = useState(0);
 const [selectedGender, setSelectedGender] = useState(null);
 const genderCatOptions = [{
    code: "F",
    name: "Women"
 }, {code: "M",
    name: "Man"
}];

 useEffect(() => {
    if (user) {
        setEmail(user.email ?? '');
        setFirstName(user.firstName ?? '');
        setLastName(user.lastName ?? '');
        setHeight(user.height ?? 0);
        setWeight(user.weight ?? 0);
        setDiploma(user.diploma ?? null);
        setSelectedGender(genderCatOptions?.filter(gender => gender.code === user.gender)?.[0] ?? null);
    }
 }, []);

 const showSuccess = () => {
    if (toast)
        toast.current.show({severity:'success', summary: 'Operation succed', detail:'Data has been modified', life: 3000});
};

const showError = () => {
    if (toast)
        toast.current.show({severity:'error', summary: 'Operation failed', detail:'Data hasn\'t been modified', life: 3000});
}

const resetExitEditMode = () => {
    setEditMode(false);
    if (user) {
        setEmail(user.email ?? '');
        setFirstName(user.firstName ?? '');
        setLastName(user.lastName ?? '');
        setHeight(user.height ?? 0);
        setWeight(user.weight ?? 0);
        setDiploma(user.diploma ?? null);
        setSelectedGender(genderCatOptions?.filter(gender => gender.code === user.gender)?.[0] ?? null);
    }
}

 const handleUpdateProfile = async () => {
    let bodyData = {}

    if (firstName !== '') {
        bodyData.firstName = firstName;
    }

    if (lastName !== '') {
        bodyData.lastName = lastName;
    }

    if (weight !== 0) {
        bodyData.weight = weight;
    }

    if (height !== 0) {
        bodyData.height = height;
    }

    if (user?.type === "trainer" && diploma) {
        bodyData.diploma = diploma;
    }

    if (selectedGender) {
        bodyData.gender = selectedGender.code;
    }

    if (Object.keys(bodyData).length > 0) {
        await setUserData("users", user.email, bodyData, { merge: true })
        .then(res => {
            if (res) {  
                setUser({
                    ...user,
                    ...bodyData
                });
                showSuccess();
            }
        })
        .catch(() => {
            showError();
        })
    }
    setEditMode(false);
 }

 const handleNextStep = async () => {
    await setUserData("users", user.email, {firstLogin: false}, { merge: true })
    .then(res => {
        if (res) {  
            setUser({
                ...user,
                firstLogin: false
            })
            if (user?.answers?.length > 0) {
                history(`/home/${user?.answers[0]}/${user?.answers[1]}`);
            } else {
                history('/home');
            }
        }
    })
 }

 const handleRemovePhoto = async (photo) => {
    await setUserData("users", user.email, {diploma: null}, { merge: true })
    .then(res => {
        if (res) {  
            setUser({
                ...user,
                diploma: null
            });
            setDiploma(null);
        }
    })
 }

 const handleUploadPhoto = async (file) => {
    setDiploma(file);
    setVisibleAddPhoto(false);
  };

  return (
    <div className="Settings align-center h-screen">
        {!user?.checkedAccount && user?.type === "trainer" && <Message severity="error" className='mb-4' text="Your account isn't checked, please wait until the admin confirm your account" />}
        <div className='grid w-10 lg:w-8 xl:w-6 gap-2 align-center border-1 border-solid border-round border-400 shadow-6'>
            <div className='col-12 align-left bg-blue-400 pl-6'>
                <h2 className='text-white'>
                    Edit Profile
                </h2>
            </div>
            <div className='col-11 grid'>
                <div className='col-12 md:col-6 lg:col-6 xl:col-6 align-left'>
                    <h5>Email</h5>
                    <p>
                        {email}
                    </p>
                </div>
                <div className='col-12 md:col-6 lg:col-6 xl:col-6 align-left'>
                    <h5>First Name</h5>
                    <span className="p-input-icon-left">
                        <i className="pi pi-user" />
                        <InputText 
                            value={firstName}
                            readOnly={!editMode}
                            onChange={(e) => setFirstName(e.target.value)} 
                            placeholder="First Name" />
                    </span>
                </div>
                <div className='col-12 md:col-6 lg:col-6 xl:col-6 align-left' style={{ justifyContent: 'flex-start' }}>
                    <h5>Last Name</h5>
                    <span className="p-input-icon-left">
                        <i className="pi pi-user" />
                        <InputText 
                            value={lastName}
                            readOnly={!editMode}
                            onChange={(e) => setLastName(e.target.value)} 
                            placeholder="Last Name" />
                    </span>
                </div>
                <div className='col-12 md:col-6 lg:col-6 xl:col-6 align-left'>
                    <h5>Gender</h5>
                    <DropdownTemplate
                        value={selectedGender} 
                        onChange={(e) => setSelectedGender(e.value)}
                        readOnly={!editMode}
                        options={genderCatOptions} 
                        optionLabel="name" 
                        placeholder="Select a option" 
                        filter
                        showClear
                        className="w-16rem"
                    /> 
                </div>
                {
                    user?.type === "trainer" &&
                    <div className='col-12 md:col-6 lg:col-6 xl:col-6 align-left'>
                        <h5>Diploma</h5>
                        {
                            editMode ? 
                                <div className="col-1" style={{position: 'relative', width: 'auto'}}>
                                        {diploma && <Button icon="pi pi-times" className="icon-btn" rounded="true" outlined="true" severity="danger" aria-label="Cancel" 
                                        style={{position: 'absolute', top: '-14%', left: '100%', transform: 'translate(-70%, 30%)', zIndex: '1'}} 
                                        onClick={() => {
                                            handleRemovePhoto(diploma)
                                        }}/>}
                                        <Image 
                                            src={diploma ?? ''}
                                            alt="Image"
                                            width='250'
                                            style={{ cursor: 'pointer', width: '250px', height: '250px' }} 
                                        />
                                </div>
                            :
                                <Image 
                                    src={diploma ?? ''}
                                    alt="Image" 
                                    width='250'
                                    preview
                                    style={{ cursor: 'pointer' }} 
                                />
                        }
                        {!diploma && <Message severity="error" text="You need to provide a diploma to continue, the field can't remain empty" />}
                        {!diploma && editMode &&
                            <>
                                <Button label={'Add Diploma'} className="icon-btn mt-4" onClick={() => setVisibleAddPhoto(true)}/>
                                <AddPhotoDialog
                                    visibleDialog={visibleAddPhoto}
                                    setVisibleDialog={setVisibleAddPhoto}
                                    onUpload={(file) => handleUploadPhoto(file)}
                                />
                            </>
                        }
                    </div>
                }
                {
                    user?.type === "champion" &&
                    <div className='col-12 md:col-6 lg:col-6 xl:col-6 align-left'>
                        <h5>Height (cm)</h5>
                        <span className="p-input-icon-left">
                            <i className="pi pi-arrows-v" />
                            <InputText 
                                value={height}
                                readOnly={!editMode}
                                onChange={(e) => setHeight(e.target.value)} 
                                placeholder="Height" />
                        </span>
                    </div>
                }

                {
                    user?.type === "champion" &&
                    <div className='col-12 md:col-6 lg:col-6 xl:col-6 align-left'>
                        <h5>Weight (Kg)</h5>
                        <span className="p-input-icon-left">
                            <i className="pi pi-arrows-h" />
                            <InputText 
                                value={weight}
                                readOnly={!editMode}
                                onChange={(e) => setWeight(e.target.value)} 
                                placeholder="Weight" />
                        </span>
                    </div>
                }
                <div className='col-10 align-left'>
                    <Button 
                        label={editMode ? 'Exit Edit Mode' : 'Edit Mode'}
                        className="icon-btn mb-2"
                        onClick={() => editMode ? resetExitEditMode() : setEditMode(true)}
                    />
                    {editMode && <Button label="Save" 
                        className="icon-btn mb-2" 
                        disabled={(isEmpty(lastName) || isEmpty(firstName) || !selectedGender || 
                            (user?.type === "trainer" && !diploma) ||
                            (user?.type === "champion" && (weight === 0 || height === 0)))}
                        onClick={() => handleUpdateProfile()} />}
                    {
                        user.firstLogin &&
                        <Button label='Next Step' 
                        disabled={ (isEmpty(lastName) || isEmpty(firstName) || !selectedGender || 
                            (user?.type === "trainer" && !diploma) ||
                            (user?.type === "champion" && (weight === 0 || height === 0))) && user?.firstLogin }
                        className="icon-btn"
                        onClick={() => handleNextStep()} />
                    }
                </div>
            </div>
            <div className="col-10">
              <Toast ref={toast} />  
            </div>
        </div>
    </div>
  );
}

export default Profile;
