import { useContext, useEffect, useRef, useState } from 'react';
import DataViewTemplate from '../components/DataViewTemplate';
import { InputText } from 'primereact/inputtext';
import { getTrainingsData } from '../services/TrainingOperations';
import { listenerChangeTrainingsData } from '../services/ListenerData';
import { UserContext } from '../contexts/UserContext';
import { Button } from 'primereact/button';
import { getCategoriesData } from '../services/CategoryOperations';
import FilterPhotos from '../components/FilterPhotos';
import AddTrainingDialog from '../components/AddTrainingDialog';
import { useParams } from 'react-router-dom';
import { Toast } from 'primereact/toast';
import { Message } from 'primereact/message';

function Home() {
  const [layoutDataView, setLayoutDataView] = useState("grid");
  const [selectedTrainer, setSelectedTrainer] = useState(null);
  const [searchValue, setSearchValue] = useState('');
  const [ratingNumber, setRatingNumber] = useState(0);
  const {userData: {user}} = useContext(UserContext);
  const [visibleUploadTrainingDialog, setVisibleUploadTrainingDialog] = useState(false);
  const {answer1, answer2} = useParams();
  const [muscleCatOptions, setMuscleCatOptions] = useState([]);
  const [trainingCatOptions, setTrainingCatOptions] = useState([]);
  const [selectedMuscleCategory, setSelectedMuscleCategory] = useState(null);
  const [selectedTrainingCategory, setSelectedTrainingCategory] = useState(null);
  const toast = useRef(null);

  const [trainingList, setTrainingList] = useState([]);

  useEffect(() => {
    setSelectedMuscleCategory(muscleCatOptions?.filter(element => element.code === answer1)?.[0] ?? null);
    setSelectedTrainingCategory(trainingCatOptions?.filter(element => element.code === answer2)?.[0] ?? null);
  }, [answer1, answer2, muscleCatOptions?.length, trainingCatOptions?.length]);

  const handleGetTrainingsData = async () => {
    if (user) {
        await getTrainingsData("trainings").then(res => {
          const auxArray = []
          res?.forEach(element => {
            if ((user?.type === "trainer" && element.data().email_trainer === user?.email && user?.checkedAccount) || user?.type !== "trainer") {
              auxArray.push({
                id: element.id,
                name: element.data().name,
                description: element?.data()?.description?.length > 30 ? element?.data()?.description.substring(0, 30) + "..." : element?.data()?.description,
                thumbnail: element.data().thumbnail,
                muscleCategory: element.data().muscleCategory,
                trainingCategory: element.data().trainingCategory,
                trainer: element.data().trainer,
                rating: element.data().rating
              })
            }
          });
          setTrainingList(auxArray);
      })
    }
  }

  const handleGetMuscleCatData = async () => {
      await getCategoriesData("muscle-category").then(res => {
        const auxArray = []
        res?.forEach(element => {
            auxArray.push({
              code: element.id,
              name: element.data().name
            })
        });
        setMuscleCatOptions(auxArray);
    })
  }

  const handleGetTrainingCatData = async () => {
    await getCategoriesData("training-category").then(res => {
      const auxArray = []
      res?.forEach(element => {
          auxArray.push({
            code: element.id,
            name: element.data().name
          })
      });
      setTrainingCatOptions(auxArray);
    })
  }

  useEffect(() => {
    listenerChangeTrainingsData("trainings", setTrainingList)
    .then(res => {
      setTrainingList(res);
    })
    .catch(error => {

    });
  }, [])

  useEffect(() => {
    handleGetTrainingsData();
    handleGetMuscleCatData();
    handleGetTrainingCatData();
  }, []);

  const showSuccess = () => {
    toast.current.clear();

    toast.current.show({
        severity:'success', 
        summary: 'Operation succeed', 
        detail:'The operation has been finished successfully! ', 
        life: 3000
    });
  }

  const showError = () => {
    toast.current.clear();
    
    toast.current.show({
        severity:'error', 
        summary: 'Login failed', 
        detail:'The password isn\'t correct', 
        life: 3000
    });
  }

  return (
    <div className="Home">
      <div className="grid p-1 m-0">
        {
          ((user?.checkedAccount && user?.type === "trainer") || user?.type !== "trainer") && 
          <div className="col-12 md:col-2 lg:col-1 mt-2 lg:mr-4">
            <FilterPhotos
              selectedMuscleCategory={selectedMuscleCategory}
              setSelectedMuscleCategory={setSelectedMuscleCategory}
              selectedTrainingCategory={selectedTrainingCategory}
              setSelectedTrainingCategory={setSelectedTrainingCategory}
              muscleCatOptions={muscleCatOptions}
              trainingCatOptions={trainingCatOptions}
              selectedTrainer={selectedTrainer}
              setSelectedTrainer={setSelectedTrainer}
              trainerOptions={Array.from(new Set(trainingList.map(train => train.trainer))).map(trainer => ({code: trainer, name: trainer}))}
              ratingNumber={ratingNumber}
              setRatingNumber={setRatingNumber}
              searchValue={searchValue}
              setSearchValue={setSearchValue}
            />
          </div>
        }

        <div className='col-12 md:col-3 lg:col-3 mt-2'>
          <span className="p-input-icon-left w-full">
              <i className="pi pi-search" />
              <InputText 
              placeholder="Type to Search..."
              className='search-input'
              value={searchValue} 
              onChange={(e) => setSearchValue(e.target.value)}
              />
          </span>
        </div>
        {!user?.checkedAccount && user?.type === "trainer" &&
          <div className='col-12'>
            <Message severity="error" text="Your account isn't checked, please wait until the admin confirm your account" />
          </div>
        }
        {
          user?.type === 'trainer' && user?.checkedAccount && 
          <div className='sm:col-12 md:col-3 lg:col-3 mt-2'>
            <Button label="Add Training" className='icon-btn' onClick={() => setVisibleUploadTrainingDialog(true)}/>
            <AddTrainingDialog
              visibleDialog={visibleUploadTrainingDialog}
              setVisibleDialog={setVisibleUploadTrainingDialog}
              muscleCategoryOptions={muscleCatOptions}
              trainingCategoryOptions={trainingCatOptions}
              onSuccess={(data) => { 
                if (data) {
                  showSuccess();
                  handleGetTrainingsData();
              } }}
              onError={(err) => {showError();}}
            />
          </div>
        }
        
        <div className="col-12">
          <DataViewTemplate
            text={"Take your best training"}
            data={
              trainingList
              .filter(element => element?.name?.toLowerCase().includes(searchValue?.toLowerCase()) && element)
              .filter(element => selectedMuscleCategory ? element?.muscleCategory === selectedMuscleCategory?.code && element : element)
              .filter(element => selectedTrainingCategory ? element?.trainingCategory === selectedTrainingCategory?.code && element : element)
              .filter(element => selectedTrainer ? element?.trainer === selectedTrainer?.name && element : element)
              .filter(element => ratingNumber !== 0 ? parseInt(element?.rating) === ratingNumber && element : element)
              .map((element) => ({...element, 
                trainingCategory: trainingCatOptions.filter(train => train.code === element.trainingCategory)?.[0]?.name,
                muscleCategory: muscleCatOptions.filter(muscle => muscle.code === element.muscleCategory)?.[0]?.name
              }))
            }
            layout={layoutDataView}
            setLayout={setLayoutDataView}
            rows={9}
          />
        </div>
      </div>
      <Toast ref={toast} />
    </div>
  );
}

export default Home;
