import React from "react";
import { useContext, useEffect, useState } from 'react';
import { UserContext } from '../contexts/UserContext';
import '../styles/Profile.css';
import { Knob } from 'primereact/knob';
import { Message } from 'primereact/message';
import { Chart } from 'primereact/chart';

function Progress() {

 const { userData: {user} } = useContext(UserContext);
 const [height, setHeight] = useState(0);
 const [weight, setWeight] = useState(0);
 const labelsDateTime = user?.type === "champion" ? Array.from(new Set(user?.exercises?.map(element => new Date(element?.date?.seconds * 1000 + element?.date?.nanoseconds / 1000000).getFullYear() + "/" + 
 new Date(element?.date?.seconds * 1000 + element?.date?.nanoseconds / 1000000).getMonth() + "/" + new Date(element?.date?.seconds * 1000 + element?.date?.nanoseconds / 1000000).getDate()))) : [];

 const caloriesPerExercise = user?.type === "champion" ? Array.from(new Set((user?.exercises ?? []).map((element) => element.id_exercise)))
        .map(exercise_id => {
            const dateTimeArr = Array.from(new Set(user?.exercises?.map(element => new Date(element?.date?.seconds * 1000 + element?.date?.nanoseconds / 1000000).getFullYear() + "/" + 
                    new Date(element?.date?.seconds * 1000 + element?.date?.nanoseconds / 1000000).getMonth() + "/" + new Date(element?.date?.seconds * 1000 + element?.date?.nanoseconds / 1000000).getDate())));

            const dataCal = user?.exercises?.filter(ex => ex.id_exercise === exercise_id && 
                dateTimeArr
                .includes(new Date(ex?.date?.seconds * 1000 + ex?.date?.nanoseconds / 1000000).getFullYear() + "/" + 
                new Date(ex?.date?.seconds * 1000 + ex?.date?.nanoseconds / 1000000).getMonth() + "/" + new Date(ex?.date?.seconds * 1000 + ex?.date?.nanoseconds / 1000000).getDate()))

            return {
            type: 'bar',
            label: user?.exercises?.filter((ex => ex.id_exercise === exercise_id))?.[0]?.name ?? "",
            backgroundColor: "#" + Math.random().toString(16).substr(-6),
            data:labelsDateTime
                .map(ex => {
                    const findMoreCal = dataCal.filter(dat => (new Date(dat?.date?.seconds * 1000 + dat?.date?.nanoseconds / 1000000).getFullYear() + "/" + 
                    new Date(dat?.date?.seconds * 1000 + dat?.date?.nanoseconds / 1000000).getMonth() + "/" + new Date(dat?.date?.seconds * 1000 + dat?.date?.nanoseconds / 1000000).getDate())
                    === ex);
                    if (findMoreCal.length > 0) {
                        return findMoreCal.map(findCal => +findCal.caloriesConsumed).reduce((accumulator, currentValue) => +accumulator + +currentValue, 0);
                    }
                    return 0;
                })
            }

        })
    : [];
 
 const weightEvolution = user?.type === "champion" ? parseFloat((((user?.weight - (labelsDateTime
    .map(element => {
        return (user?.exercises
        .filter(item => new Date(item?.date?.seconds * 1000 + item?.date?.nanoseconds / 1000000).getFullYear() + "/" + 
        new Date(item?.date?.seconds * 1000 + item?.date?.nanoseconds / 1000000).getMonth() + "/" + new Date(item?.date?.seconds * 1000 + item?.date?.nanoseconds / 1000000).getDate() === element)
        .map(item => +item.caloriesConsumed)
        .reduce((accumulator, currentValue) => accumulator + currentValue, 0) / 7700)
    })
    .reduce((accumulator, currentValue) => +accumulator + +currentValue, 0))) / user?.height) / user?.height) * 10000).toFixed(1) : [];

 useEffect(() => {
    if (user) {
        setHeight(user.height ?? 0);
        setWeight(user.weight ?? 0);
    }
 }, []);

  return (
    <div className="Settings align-center">
        <div className='grid p-2 m-0'>
            <div className='col-12 md:col-6 lg:col-6 xl:col-6 border-solid border-1 border-300 border-round align-center'>
                <h3>Height (cm)</h3>
                <p className="text-sm md:text-base lg:text-lg xl:text-xl">
                    {height}
                </p>
            </div>

            <div className='col-12 md:col-6 lg:col-6 xl:col-6 border-solid border-1 border-300 border-round align-center'>
                <h3>Initial Weight (kg)</h3>
                <p className="text-sm md:text-base lg:text-lg xl:text-xl">
                    {weight}
                </p>
            </div>

            <div className='col-12 md:col-6 lg:col-6 xl:col-6 border-solid border-1 border-300 border-round align-center'>
                <h3>Fat percentage</h3>
                <Knob 
                    value={weightEvolution} 
                    min={0} 
                    max={100}
                    valueColor={weightEvolution > 30 ? "#ED2939" 
                    : weightEvolution >= 25 && 
                    weightEvolution <= 30 ? "#FFA500" : "#00FF00"}
                />
                {
                    weightEvolution > 30 ?
                        <Message severity="error" text="Your fat percentage is to high, you have obesity" />
                    :
                    weightEvolution >= 25 && 
                    weightEvolution <= 30 ?
                        <Message severity="warn" text="You are overweight" />
                    :
                        <Message severity="success" text="You have a normal weight" />
                }
            </div>

            <div className='col-12 md:col-6 lg:col-6 xl:col-6 border-solid border-1 border-300 border-round align-center'>
                <h3>Calories consumed</h3>
                <Chart 
                    type="bar" 
                    width='80%' 
                    className="align-center"
                    data={{
                        labels: labelsDateTime,
                        datasets: [
                            {
                                label: 'Calories',
                                data: labelsDateTime
                                    .map(element => {
                                        return user?.exercises
                                        .filter(item => new Date(item?.date?.seconds * 1000 + item?.date?.nanoseconds / 1000000).getFullYear() + "/" + 
                                        new Date(item?.date?.seconds * 1000 + item?.date?.nanoseconds / 1000000).getMonth() + "/" + new Date(item?.date?.seconds * 1000 + item?.date?.nanoseconds / 1000000).getDate() === element)
                                        .map(item => item.caloriesConsumed)
                                        .reduce((accumulator, currentValue) => accumulator + currentValue, 0)
                                    }),
                                backgroundColor: 'rgba(255, 159, 64, 0.2)',
                                borderColor: 'rgb(255, 159, 64)',
                                borderWidth: 1
                            }
                        ]
                    }} 
                    options={{
                        scales: {
                            y: {
                                beginAtZero: true,
                            }
                        }
                    }} 
                />
            </div>
            {/* <div className='col-12 md:col-6 lg:col-6 xl:col-6 border-solid border-1 border-300 border-round align-center'>
                <h3>Kilograms evolution</h3>
                <Chart 
                    type="bar" 
                    width='80%'
                    className="align-center"
                    data={{
                        labels: labelsDateTime,
                        datasets: [
                            {
                                label: 'Kilograms',
                                data: labelsDateTime
                                    .map(element => {
                                        return (user?.weight - (user?.exercises
                                        .filter(item => new Date(item?.date?.seconds * 1000 + item?.date?.nanoseconds / 1000000).getFullYear() + "/" + 
                                        new Date(item?.date?.seconds * 1000 + item?.date?.nanoseconds / 1000000).getMonth() + "/" + new Date(item?.date?.seconds * 1000 + item?.date?.nanoseconds / 1000000).getDate() === element)
                                        .map(item => +item.caloriesConsumed)
                                        .reduce((accumulator, currentValue) => accumulator + currentValue, 0) / 7700))
                                    }),
                                backgroundColor: 'rgba(75, 192, 192, 0.2)',
                                borderColor: 'rgb(75, 192, 192)',
                                borderWidth: 1
                            }
                        ]
                    }} 
                    options={{
                        scales: {
                            y: {
                                beginAtZero: true,
                            }
                        }
                    }} 
                />
            </div> */}
            <div className='col-12 md:col-6 lg:col-6 xl:col-6 border-solid border-1 border-300 border-round align-center'>
                <h3>Calories per exercise</h3>
                <Chart 
                    type="bar"
                    width='80%'
                    height='600px'
                    className="align-center"
                    data={{
                        labels: labelsDateTime,
                        datasets: caloriesPerExercise
                                    
                    }} 
                    options={{
                        maintainAspectRatio: false,
                        aspectRatio: 0.8,
                        plugins: {
                            tooltips: {
                                mode: 'index',
                                intersect: false
                            },
                            legend: {
                                labels: {
                                    color: "#000"
                                }
                            }
                        },
                        scales: {
                            x: {
                                stacked: true,
                                ticks: {
                                    color: "#000"
                                },
                                grid: {
                                    color: "#dadada"
                                }
                            },
                            y: {
                                stacked: true,
                                ticks: {
                                    color: "#000"
                                },
                                grid: {
                                    color: "#dadada"
                                }
                            }
                        }
                    }} 
                />
            </div>
        </div>
    </div>
  );
}

export default Progress;

