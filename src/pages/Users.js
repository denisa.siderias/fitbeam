import { Button } from "primereact/button";
import { Column } from "primereact/column";
import { DataTable } from "primereact/datatable";
import { Image } from "primereact/image";
import { Toast } from "primereact/toast";
import React, { useEffect, useRef, useState } from "react";
import { listenerChangeUsersData } from "../services/ListenerData";
import { deleteUserData, getUsersData, setUserData } from "../services/UserOperations";

const Users = () => {
    const toast = useRef(null);
    const [users, setUsers] = useState([]);
   
    const showSuccess = () => {
       if (toast)
           toast.current.show({severity:'success', summary: 'Operation succed', detail:'Data has been modified', life: 3000});
    };
   
    const handleGetUsersDataData = async () => {
        await getUsersData("users").then(res => {
            const auxArray = []
            res?.forEach(element => {
                auxArray.push(element.data());
            });
            setUsers(auxArray);
        });
    }
   
   
   const handleDeleteUser = async (email) => {
       await deleteUserData("users", email).then(res => {
           showSuccess();
       });
   }
   
   const handleVerifyUser = async (email) => {
       await setUserData("users", email, { checkedAccount: true }, { merge: true })
       .then(res => {
           if (res) {  
               showSuccess();
           }
       })
   }
   
    useEffect(() => {
       handleGetUsersDataData();
    }, []);
   
    useEffect(() => {
        listenerChangeUsersData("users")
       .then(res => {
           if (res) {
               const auxArray = []
               res?.forEach(element => {
                   auxArray.push(element.data());
               });
               setUsers(auxArray);
           }
       })
    });

    const championBodyTemplate = (rowData) => {
        return (
            <React.Fragment>
                <Button label="Delete" className="icon-btn" onClick={() => handleDeleteUser(rowData.email)} />
            </React.Fragment>
        );
     };
     
     const trainerBodyTemplate = (rowData) => {
        return (
            <React.Fragment>
                <Button label="Check account" disabled={ rowData.checkedAccount } className="icon-btn mr-2" onClick={() => handleVerifyUser(rowData.email)} />
                <Button label="Delete" className="icon-btn" onClick={() => handleDeleteUser(rowData.email)} />
            </React.Fragment>
        );
     };

     const trainerDiplomaBodyTemplate = (rowData) => {
        return (
            <Image src={rowData.diploma} alt={rowData.email} width="150" preview />
        );
     }

    return (
        <div className="align-center w-full p-2">
            <h2>Champions</h2>
            <DataTable value={(users ?? []).filter(element => element?.type === "champion")}
            tableStyle={{ width: '100%' }}>
                <Column field="firstName" header="First Name"></Column>
                <Column field="lastName" header="Last Name"></Column>
                <Column field="email" header="Email"></Column>
                <Column field="firstLogin" header="First Login"></Column>
                <Column body={ championBodyTemplate } header="Action" exportable={false} style={{ minWidth: '5rem' }}></Column>
            </DataTable>
            <h2 className='mt-6'>Trainers</h2>
            <DataTable value={(users ?? []).filter(element => element?.type === "trainer")}
                tableStyle={{ width: '100%' }}>
            <Column field="firstName" header="First Name"></Column>
                <Column field="lastName" header="Last Name"></Column>
                <Column field="email" header="Email"></Column>
                <Column body={ trainerDiplomaBodyTemplate } header="Diploma"></Column>
                <Column body={ trainerBodyTemplate } header="Action" exportable={false} style={{ minWidth: '5rem' }}></Column>
            </DataTable>
            <Toast ref={toast} />
        </div>
    );
};

export default Users;