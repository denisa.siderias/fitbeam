import { Button } from 'primereact/button';
import { Message } from 'primereact/message';
import React, { useContext, useEffect, useState } from 'react';
import DropdownTemplate from '../components/DropdownTemplate';
import { RoutingContext } from '../contexts/RoutingContext';
import { UserContext } from '../contexts/UserContext';
import { getCategoriesData } from '../services/CategoryOperations';
import { setUserData } from '../services/UserOperations';

const QuestionForm = () => {

    const { userData: {user, setUser} } = useContext(UserContext);
    const { history } = useContext(RoutingContext);
    const [muscleCatOptions, setMuscleCatOptions] = useState([]);
    const [trainingCatOptions, setTrainingCatOptions] = useState([]);
    const [selectedMuscle, setSelectedMuscle] = useState(null);
    const [selectedTraining, setSelectedTraining] = useState(null);

    const handleGetMuscleCatData = async () => {
        await getCategoriesData("muscle-category").then(res => {
          const auxArray = []
          res?.forEach(element => {
              auxArray.push({
                code: element.id,
                name: element.data().name
              })
          });
          setMuscleCatOptions(auxArray);
      })
    }

    const handleGetTrainingCatData = async () => {
        await getCategoriesData("training-category").then(res => {
          const auxArray = []
          res?.forEach(element => {
              auxArray.push({
                code: element.id,
                name: element.data().name
              })
          });
          setTrainingCatOptions(auxArray);
      })
    }

    useEffect(() => {
        handleGetMuscleCatData();
        handleGetTrainingCatData();
    }, []);

    const handleAnswerQuestions = async () => {
        if (selectedMuscle && selectedTraining) {
            let bodyData = {
                answers: [selectedMuscle.code, selectedTraining.code],
                answerQuest: true
            }
        
            if (Object.keys(bodyData).length > 0) {
                await setUserData("users", user.email, bodyData, { merge: true })
                .then(res => {
                    if (res) {  
                            setUser({
                                ...user,
                                ...bodyData
                            })
                        history('/profile');
                    }
                })
            }
        }
    }

    return (
        <div className='align-center h-screen w-full question-form p-2'>
            <div className='border-round shadow-2 p-4 mb-4 w-8 md:w-6 lg:w-4 xl:w-4 question'>
                <h2>Muscle*</h2>
                <p className='mb-4'>Please select the muscle you are interested to work more.</p>
                <DropdownTemplate
                        value={selectedMuscle} 
                        onChange={(e) => setSelectedMuscle(e.value)} 
                        options={muscleCatOptions} 
                        optionLabel="name" 
                        placeholder="Select a option" 
                        filter
                        showClear
                        className="w-full"
                /> 
            </div>
            <div className='border-round shadow-2 p-4 w-8 md:w-6 lg:w-4 xl:w-4 question mb-4'>
                <h2>Training Type*</h2>
                <p className='mb-4'>Please select the training type you liked.</p>
                <DropdownTemplate
                    value={selectedTraining} 
                    onChange={(e) => setSelectedTraining(e.value)} 
                    options={trainingCatOptions} 
                    optionLabel="name" 
                    placeholder="Select a option" 
                    filter
                    showClear
                    className="w-full"
                />
            </div>
            <Message severity="info" text="* - mandatory field" className='mb-4 w-8 md:w-6 lg:w-4 xl:w-4' />
            <Button label="Next Step" disabled={!selectedTraining || !selectedTraining} className='icon-btn' onClick={() => handleAnswerQuestions()} />
        </div>
    );
};

export default QuestionForm;