import React, { useContext, useEffect, useRef, useState } from "react";
import { Image } from 'primereact/image';
import { getTrainingsData, setTrainingData } from "../services/TrainingOperations";
import { useParams } from "react-router-dom";
import { Rating } from "primereact/rating";
import { Button } from "primereact/button";
import { listenerChangeTrainingsData } from "../services/ListenerData";
import DataViewTemplate from "../components/DataViewTemplate";
import { InputText } from "primereact/inputtext";
import { Toast } from "primereact/toast";
import { RoutingContext } from "../contexts/RoutingContext";
import Muscle from "../images/muscle.png";
import Delete from "../images/delete.png";
import { getCategoriesData } from "../services/CategoryOperations";
import FilterPhotos from "../components/FilterPhotos";

const Favorites = () => {
    const [favoritesList, setFavoritesList] = useState(null);
    const toast = useRef(null);
    const {userName} = useParams();
    const { history } = useContext(RoutingContext);
    const [selectedTrainer, setSelectedTrainer] = useState(null);
    const [searchValue, setSearchValue] = useState('');
    const [ratingNumber, setRatingNumber] = useState(0);
    const [muscleCatOptions, setMuscleCatOptions] = useState([]);
    const [trainingCatOptions, setTrainingCatOptions] = useState([]);
    const [selectedMuscleCategory, setSelectedMuscleCategory] = useState(null);
    const [selectedTrainingCategory, setSelectedTrainingCategory] = useState(null);

    const handleGetMuscleCatData = async () => {
        await getCategoriesData("muscle-category").then(res => {
          const auxArray = []
          res?.forEach(element => {
              auxArray.push({
                code: element.id,
                name: element.data().name
              })
          });
          setMuscleCatOptions(auxArray);
      })
    }
  
    const handleGetTrainingCatData = async () => {
      await getCategoriesData("training-category").then(res => {
        const auxArray = []
        res?.forEach(element => {
            auxArray.push({
              code: element.id,
              name: element.data().name
            })
        });
        setTrainingCatOptions(auxArray);
      })
    }

    const handleGetFavoritesTraining = async (userName) => {
        await getTrainingsData("favorites").then(res => {
            const auxArray = []
            res?.forEach(element => {
                if (element?.id === userName) {
                    const data = element?.data();
                    data?.trainings?.map(item => {
                        auxArray.push({
                            ...item
                        });
                    })
                }  
            });
            setFavoritesList(auxArray);
        })
    }

    useEffect(() => {
        listenerChangeTrainingsData("favorites", setFavoritesList)
        .then(res => {
            if (res)
                setFavoritesList([]);
        })
        .catch(error => {

        });
      }, [])

    useEffect(() => {
        if (userName && favoritesList?.length === 0) {
            handleGetTrainingCatData();
            handleGetMuscleCatData();
            handleGetFavoritesTraining(userName);
        }
    }, [favoritesList?.length]);

    const handleDeleteFavoritesTraining = async (userName, id_training) => {
        await setTrainingData("favorites", userName, {
            trainings: favoritesList?.filter(element => element?.id !== id_training)
        }, { merge: true }).then(res => {
            if (res) {
                showSuccess();
                setFavoritesList([]);
            }
        })
        .catch(err => {
            showError();
        })
    }

    const showSuccess = () => {
        if (toast)
            toast.current.show({severity:'success', summary: 'Operation succed', detail:'Data has been modified', life: 3000});
    };

    const showError = () => {
        if (toast)
            toast.current.show({severity:'error', summary: 'Operation failed', detail:'Data hasn\'t been modified', life: 3000});
    }

    return (
        <div className="grid p-1 m-0">
            <div className="col-12 md:col-2 lg:col-1 mt-2 lg:mr-4">
                <FilterPhotos
                    selectedMuscleCategory={selectedMuscleCategory}
                    setSelectedMuscleCategory={setSelectedMuscleCategory}
                    selectedTrainingCategory={selectedTrainingCategory}
                    setSelectedTrainingCategory={setSelectedTrainingCategory}
                    muscleCatOptions={muscleCatOptions}
                    trainingCatOptions={trainingCatOptions}
                    selectedTrainer={selectedTrainer}
                    setSelectedTrainer={setSelectedTrainer}
                    trainerOptions={Array.from(new Set(favoritesList?.map(train => train.trainer))).map(trainer => ({code: trainer, name: trainer}))}
                    ratingNumber={ratingNumber}
                    setRatingNumber={setRatingNumber}
                    searchValue={searchValue}
                    setSearchValue={setSearchValue}
                />
            </div>
            <div className='col-12 md:col-3 lg:col-3 mt-2'>
            <span className="p-input-icon-left w-full">
                <i className="pi pi-search" />
                <InputText 
                placeholder="Type to Search..."
                className='search-input'
                value={searchValue} 
                onChange={(e) => setSearchValue(e.target.value)}
                />
            </span>
            </div>
            <div className="col-12">
                <DataViewTemplate
                text={"Here are your favorite trainings"}
                data={(favoritesList ?? [])
                    .filter(element => element?.name?.toLowerCase().includes(searchValue?.toLowerCase()) && element)
                    .filter(element => selectedMuscleCategory ? element?.muscleCategory === selectedMuscleCategory?.code && element : element)
                    .filter(element => selectedTrainingCategory ? element?.trainingCategory === selectedTrainingCategory?.code && element : element)
                    .filter(element => selectedTrainer ? element?.trainer === selectedTrainer?.name && element : element)
                    .filter(element => ratingNumber !== 0 ? parseInt(element?.rating) === ratingNumber && element : element)
                    .map((element) => ({...element, 
                        trainingCategory: trainingCatOptions.filter(train => train.code === element.trainingCategory)?.[0]?.name,
                        muscleCategory: muscleCatOptions.filter(muscle => muscle.code === element.muscleCategory)?.[0]?.name
                      }))
                    .sort((firstElement, secondElement) => {
                    if (firstElement?.name?.toLowerCase() < secondElement?.name?.toLowerCase())
                        return -1;
                    
                    if (firstElement?.name?.toLowerCase() > secondElement?.name?.toLowerCase())
                        return 1;
                    
                    return 0;
                }) ?? []}
                layout="list"
                renderListItem={(data) => (<div className="col-12">
                    <div className="flex flex-column align-items-center p-3 w-full md:flex-row">
                        <Image src={data?.thumbnail} alt="Image" width="250" height="180" />
                        <div className="text-center md:text-left md:flex-1 ml-3">
                            <div className="text-2xl font-bold mb-2">{data?.name}</div>
                            <div className="text-lg mb-2">{data?.trainer}</div>
                            <i className="pi pi-tag vertical-align-middle mr-2"></i>
                            <span className="vertical-align-middle font-semibold">{data?.muscleCategory}</span>
                            <div className="mb-3">{data?.description.length > 30 ? data?.description?.substring(0, 100) + "..." : data?.description}</div>
                            <Rating className="mb-2" value={data?.rating} readOnly cancel={false}></Rating>
                            <i className="pi pi-tag vertical-align-middle mr-2"></i>
                            <span className="vertical-align-middle font-semibold">{data?.trainingCategory}</span>
                        </div>
                        <div className="flex flex-column mt-5 justify-content-between align-items-center md:w-auto w-full">
                            <Button className="mb-2 icon-btn" icon={<img src={Muscle} alt="muscle" width="20" className='mr-2' />} label="View Training" onClick={() => history(`/view-training/${data?.id}`)}></Button>
                            <Button className="mb-2 icon-btn md:w-full lg:w-full" icon={<img src={Delete} alt="muscle" width="20" className='mr-2' />} label="Unfavorite" onClick={() => handleDeleteFavoritesTraining(userName, data?.id)}></Button>
                        </div>
                    </div>
                </div>)}
                rows={9}
                />
            </div>
            <div className="col-12">
              <Toast ref={toast} />  
            </div>
        </div>
    )
}

export default Favorites;