import React from "react";
import { Button } from 'primereact/button';
import { InputText } from 'primereact/inputtext';
import { useEffect, useRef, useState } from 'react';
import { setUserData } from '../services/UserOperations';
import '../styles/Profile.css';
import { Toast } from 'primereact/toast';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { getCategoriesData } from '../services/CategoryOperations';
import { deleteTrainingData } from '../services/TrainingOperations';

function Categories() {
 const toast = useRef(null);
 const [muscleType, setMuscleType] = useState('');
 const [muscleName, setMuscleName] = useState('');
 const [trainingType, setTrainingType] = useState('');
 const [trainingName, setTrainingName] = useState('');
 const [muscleCat, setMuscleCat] = useState([]);
 const [trainingCat, setTrainingCat] = useState([]);

 const showSuccess = () => {
    if (toast)
        toast.current.show({severity:'success', summary: 'Operation succed', detail:'Data has been modified', life: 3000});
};

const showError = () => {
    if (toast)
        toast.current.show({severity:'error', summary: 'Operation failed', detail:'Data hasn\'t been modified', life: 3000});
}

 const handleAddMuscleCat = async () => {
    let bodyData = {}

    if (muscleName !== '') {
        bodyData.name = muscleName;
    }

    if (Object.keys(bodyData).length > 0 && muscleType !== '') {
        await setUserData("muscle-category", muscleType, bodyData, { merge: true })
        .then(res => {
            if (res) {  
                showSuccess();
                setMuscleCat([...muscleCat, {muscleType: muscleType, muscleName: muscleName}]);
                setMuscleName("");
                setMuscleType("");
            }
        })
        .catch(() => {
            showError();
        })
    }
 }

 const handleAddTrainingCat = async () => {
    let bodyData = {}

    if (trainingName !== '') {
        bodyData.name = trainingName;
    }

    if (Object.keys(bodyData).length > 0 && trainingType !== '') {
        await setUserData("training-category", trainingType, bodyData, { merge: true })
        .then(res => {
            if (res) {  
                showSuccess();
                setTrainingCat([...trainingCat, {trainingType: trainingType, trainingName: trainingName}]);
                setTrainingName("");
                setTrainingType("");
            }
        })
        .catch(() => {
            showError();
        })
    }
 }

    const handleGetMuscleCatData = async () => {
        await getCategoriesData("muscle-category").then(res => {
            const auxArray = []
            res?.forEach(element => {
                auxArray.push({
                    muscleType: element.id,
                    muscleName: element.data().name
                });
            });
            setMuscleCat(auxArray);
        });
    }

    const handleGetTrainingCatData = async () => {
        await getCategoriesData("training-category").then(res => {
            const auxArray = []
            res?.forEach(element => {
                auxArray.push({
                    trainingType: element.id,
                    trainingName: element.data().name
                });
            });
            setTrainingCat(auxArray);
        });
    }

const handleDeleteTrainingCat = async (trainingType) => {
    await deleteTrainingData("training-category", trainingType).then(res => {
        showSuccess();
        setTrainingCat(trainingCat.filter(element => element.trainingType !== trainingType));
    });
}

const handleDeleteMuscleCat = async (muscleType) => {
    await deleteTrainingData("muscle-category", muscleType).then(res => {
        showSuccess();
        setMuscleCat(muscleCat.filter(element => element.muscleType !== muscleType));
    })
}

 useEffect(() => {
    handleGetMuscleCatData();
    handleGetTrainingCatData();
 }, []);

 const muscleBodyTemplate = (rowData) => {
    return (
        <React.Fragment>
            <Button icon="pi pi-trash" severity="danger" onClick={() => handleDeleteMuscleCat(rowData.muscleType)} />
        </React.Fragment>
    );
 };

 const trainingBodyTemplate = (rowData) => {
    return (
        <React.Fragment>
            <Button icon="pi pi-trash" severity="danger" onClick={() => handleDeleteTrainingCat(rowData.trainingType)} />
        </React.Fragment>
    );
 };

  return (
    <div className="Settings align-center">
        <h2>Muscle Category</h2>
        <DataTable value={muscleCat} tableStyle={{ minWidth: '50rem' }}>
            <Column field="muscleType" header="Muscle Type"></Column>
            <Column field="muscleName" header="Muscle Name"></Column>
            <Column body={ muscleBodyTemplate } header="Action" exportable={false} style={{ minWidth: '5rem' }}></Column>
        </DataTable>
        <h2 className='mt-4'>Training Category</h2>
        <DataTable value={trainingCat} tableStyle={{ minWidth: '50rem' }}>
            <Column field="trainingType" header="Training Type"></Column>
            <Column field="trainingName" header="Training Name"></Column>
            <Column body={ trainingBodyTemplate } header="Action" exportable={false} style={{ minWidth: '5rem' }}></Column>
        </DataTable>
        <div className='mt-4 grid w-10 lg:w-8 xl:w-6 gap-2 align-center border-1 border-solid border-round border-400 shadow-6 mb-8'>
            <div className='col-12 align-left bg-blue-400 pl-6'>
                <h2 className='text-white'>
                    Muscle Category
                </h2>
            </div>
            <div className='col-11 grid'>
                <div className='col-12 md:col-6 lg:col-6 xl:col-6 align-left'>
                    <h5>Muscle type</h5>
                    <span className="p-input-icon-left">
                        <i className="pi pi-book" />
                        <InputText 
                            value={muscleType}
                            onChange={(e) => setMuscleType(e.target.value)} 
                            placeholder="Muscle type" />
                    </span>
                </div>
                <div className='col-12 md:col-6 lg:col-6 xl:col-6 align-left' style={{ justifyContent: 'flex-start' }}>
                    <h5>Muscle name</h5>
                    <span className="p-input-icon-left">
                        <i className="pi pi-id-card" />
                        <InputText 
                            value={muscleName}
                            onChange={(e) => setMuscleName(e.target.value)} 
                            placeholder="Muscle name" />
                    </span>
                </div>
            </div>
            <div className='col-11 mb-2'>
                <Button label="Add muscle category" className='icon-btn' onClick={() => handleAddMuscleCat()} />
            </div>
        </div>
        <div className='mb-4 grid w-10 lg:w-8 xl:w-6 gap-2 align-center border-1 border-solid border-round border-400 shadow-6'>
            <div className='col-12 align-left bg-blue-400 pl-6'>
                <h2 className='text-white'>
                    Training Category
                </h2>
            </div>
            <div className='col-11 grid'>
                <div className='col-12 md:col-6 lg:col-6 xl:col-6 align-left'>
                    <h5>Training type</h5>
                    <span className="p-input-icon-left">
                        <i className="pi pi-book" />
                        <InputText 
                            value={trainingType}
                            onChange={(e) => setTrainingType(e.target.value)} 
                            placeholder="Training type" />
                    </span>
                </div>
                <div className='col-12 md:col-6 lg:col-6 xl:col-6 align-left' style={{ justifyContent: 'flex-start' }}>
                    <h5>Training name</h5>
                    <span className="p-input-icon-left">
                        <i className="pi pi-id-card" />
                        <InputText 
                            value={trainingName}
                            onChange={(e) => setTrainingName(e.target.value)} 
                            placeholder="Training name" />
                    </span>
                </div>
            </div>
            <div className='col-11 mb-2'>
                <Button label="Add training category" className='icon-btn' onClick={() => handleAddTrainingCat()} />
            </div>
        </div> 
        <Toast ref={toast} />
    </div>
  );
}

export default Categories;
