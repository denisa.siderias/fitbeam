import '../styles/App.css';
import Login from './Login';
import {
  Routes,
  Route
} from "react-router-dom";
import ProtectedRoute from '../components/ProtectedRoute';
import FourOFour from './FourOfFour';
import Home from './Home';
import Register from './Register';
import Profile from './Profile';
import { getAuth, onAuthStateChanged } from 'firebase/auth';
import { getUserData } from '../services/UserOperations';
import { useContext, useEffect } from 'react';
import { UserContext } from '../contexts/UserContext';
import { RoutingContext } from '../contexts/RoutingContext';
import Navbar from '../components/Navbar';
import ViewTraining from './ViewTraining';
import Favorites from './Favorites';
import QuestionForm from './QuestionForm';
import Progress from './Progress';
import Statistics from './Statistics';
import Categories from './Categories';
import ChatMessages from './ChatMessages';
import Users from './Users';

function App() {
  const { userData: { setUser } } = useContext(UserContext);
  const { history, location } = useContext(RoutingContext);
  const auth = getAuth();

  useEffect(() => {
    onAuthStateChanged(auth, (user) => {
      if (user) {
          getUserData("users", user.email)
          .then(res => {
              res.email = user.email;
              setUser({
                  ...res
              });
              if (res?.type === "champion" && !res?.answerQuest) {
                history('/question-form');
              } else if (res?.firstLogin) {
                  history('/profile');
              } else {
                  if (location.pathname === "/")
                    if (res?.answers?.length > 0) {
                      history(`/home/${res?.answers[0]}/${res?.answers[1]}`);
                    } else {
                      history('/home');
                    }
                  else
                    history(location.pathname);
              }
          });
      } else {
        setUser(null);
      }
    });
  }, [])

  return (
    <div className="App">
        {auth.currentUser && location.pathname !== "/question-form" && <Navbar labelAvatar={auth?.currentUser?.email[0]?.toUpperCase()} />}
        <Routes>
          <Route exact path='/home/:answer1/:answer2' element={<ProtectedRoute path='/home/:answer1/:answer2'><Home /></ProtectedRoute>}/>
          <Route exact path='/home' element={<ProtectedRoute path='/home'><Home /></ProtectedRoute>}/>
          <Route exact path='/profile' element={<ProtectedRoute path='/profile'><Profile /></ProtectedRoute>}/>
          <Route exact path='/progress' element={<ProtectedRoute path='/progress'><Progress /></ProtectedRoute>}/>
          <Route exact path='/statistics' element={<ProtectedRoute path='/statistics'><Statistics /></ProtectedRoute>}/>
          <Route exact path='/categories' element={<ProtectedRoute path='/categories'><Categories /></ProtectedRoute>}/>
          <Route exact path='/question-form' element={<ProtectedRoute path='/question-form'><QuestionForm /></ProtectedRoute>} />
          <Route exact path='/view-training/:training' element={<ProtectedRoute path='/view-training/:training'><ViewTraining /></ProtectedRoute>}/>
          <Route exact path='/favorites/:userName' element={<ProtectedRoute path='/favorites/:userName'><Favorites /></ProtectedRoute>}/>
          <Route exact path='/chatmessages/:nameConversation' element={<ProtectedRoute path='/chatmessages/:nameConversation'><ChatMessages /></ProtectedRoute>}/>
          <Route exact path='/chatmessages' element={<ProtectedRoute path='/chatmessages'><ChatMessages /></ProtectedRoute>}/>
          <Route exact path='/users' element={<ProtectedRoute path='/users'><Users /></ProtectedRoute>}/>
          <Route exact path='/fourofour' element={<FourOFour />} />
          <Route exact path='/' element={<Login />} />
          <Route exact path='/register' element={<Register />} />
        </Routes>
    </div>
  );
}

export default App;
