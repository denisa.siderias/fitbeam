import PasswordTemplate from '../components/PasswordTemplate';
import { setUserData } from '../services/UserOperations';
import { Password } from 'primereact/password';
import '../styles/Register.css';
import { InputText } from 'primereact/inputtext';
import { useRef, useState } from 'react';
import { Button } from 'primereact/button';
import { Link } from 'react-router-dom';
import { Dropdown } from 'primereact/dropdown';
import { Toast } from 'primereact/toast';
import { getAuth, createUserWithEmailAndPassword } from "firebase/auth";
import AddPhotoDialog from '../components/AddPhotoDialog';
import { Image } from 'primereact/image';
import Muscle from "../images/muscle_black.png";
import { Timestamp } from 'firebase/firestore';

function Register() {

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [selectedType, setSelectedType] = useState(null);
  const [visibleAddPhoto, setVisibleAddPhoto] = useState(false);
  const [diploma, setDiploma] = useState(null);
  const client_type = [
    { name: 'Trainer', code: 'trainer' },
    { name: 'Champion', code: 'champion' }
  ];
  const auth = getAuth();
  const toast = useRef(null);

  const showSuccess = () => {
    toast.current.clear();

    toast.current.show({
        severity:'success', 
        summary: 'Register with success', 
        detail:'You can now go to log in and try the app', 
        life: 3000
    });
  }

  const showWarn = () => {
    toast.current.clear();

    toast.current.show({
        severity:'warn', 
        summary: 'Account exists', 
        detail:'Exist an account with this email, try to log in', 
        life: 3000
    });
  }

  const showError = () => {
    toast.current.clear();
    
    toast.current.show({
        severity:'error', 
        summary: 'Register failed', 
        detail:'The account can\'t be created', 
        life: 3000
    });
  }

  const handleRegister = () => {
    if (email && password && (selectedType || (selectedType === "trainer" && diploma))) {
        createUserWithEmailAndPassword(auth, email, password)
        .then(async () => {
            let bodyData = {
                firstLogin: true,
                firstName: "",
                lastName: "",
                gender: "",
                isLogged: false,
                lastLogged: Timestamp.now(),
                email: email,
                type: selectedType,
                permissions: selectedType === 'champion' ? ['/home', '/profile', '/view-training', '/favorites', '/question-form', '/progress', "/chatmessages"] : 
                                        selectedType === "admin" ? ['/home', '/view-training', '/profile', '/categories', '/users'] :
                                        ['/home', '/profile', '/view-training', '/statistics', '/chatmessages'],
            };
            if (selectedType === "trainer") {
                bodyData.diploma = diploma;
                bodyData.checkedAccount = false;
            } else if (selectedType === "champion") {
                bodyData.answerQuest = false;
                bodyData.age = 0;
                bodyData.height = 0;
                bodyData.weight = 0;
                bodyData.exercises = [];
                bodyData.answers = [];
            }
            await setUserData("users", email, bodyData).then(res => {
                if (res) {
                    setEmail('');
                    setPassword('');
                    setSelectedType(null);
                    setDiploma(null);
                    showSuccess();
                } else {
                    showError();
                }
            })
        })
        .catch((error) => {
            if (error.code === "auth/email-already-in-use") {
                showWarn();
            } else {
                showError();
            }
        });
    }
  }

  const userTypeOptionTemplate = (option) => {
    return (
        <div className="align-center" style={{flexDirection: 'row'}}>
            {
                option.code === "trainer" ?
                    <img src={Muscle} alt="muscle" width="20" className='mr-2' />
                :
                    <i className={'pi pi-heart'} />
            }
            <div className='margin-left-sm'>{option.name}</div>
        </div>
    );
  }

  const selectedUserTypeTemplate = (option, props) => {
    if (option) {
        return (
            <div className="align-center" style={{flexDirection: 'row'}}>
                {
                    option.code === "trainer" ?
                        <img src={Muscle} alt="muscle" width="20" className='mr-2' />
                    :
                        <i className={'pi pi-heart'} />
                }
                <div className='margin-left-sm'>{option.name}</div>
            </div>
        );
    }

    return (
        <span>
            {props.placeholder}
        </span>
    );
  }

  const handleUploadPhoto = async (file) => {
    setDiploma(file);
    setVisibleAddPhoto(false);
  };

  return (
    <div className="Register align-center">
        <div className='form-data align-center shadow-6'>
            <div className='align-left'>
                <h3>Email</h3>
                <span className="p-input-icon-right">
                    <i className="pi pi-envelope" />
                    <InputText value={email} onChange={(e) => setEmail(e.target.value)} placeholder="Email" />
                </span>
            </div>
            <div className='align-left margin-bottom-between'>
                <h3>Account type</h3>
                <Dropdown 
                    className='w-16rem'
                    value={selectedType} 
                    options={client_type} 
                    onChange={(e) => setSelectedType(e.value)} 
                    optionLabel="name"
                    optionValue='code'
                    itemTemplate={userTypeOptionTemplate}
                    valueTemplate={selectedUserTypeTemplate}
                    placeholder="Select a user type" />
            </div>
            {
                selectedType === "trainer" &&
                <div className='align-left margin-bottom-between'>
                    <h3>Diploma*</h3>
                    {
                        diploma ?
                            <>
                                <Button
                                    label="Cancel upload"
                                    className='p-button-raised p-button-info p-button-raised p-button-info bg-white text-blue-400 mb-2'
                                    onClick={() => setDiploma(null)} />
                                <Image src={diploma} alt="Diploma" width="250" />
                            </>
                        :
                            <>
                                <Button
                                    label={'Add Diploma'}
                                    className="p-button-raised p-button-info p-button-raised p-button-info bg-white text-blue-400"
                                    onClick={() => setVisibleAddPhoto(true)}/>
                                <AddPhotoDialog
                                    visibleDialog={visibleAddPhoto}
                                    setVisibleDialog={setVisibleAddPhoto}
                                    onUpload={(file) => handleUploadPhoto(file)}
                                />
                            </>
                    }
                    
                </div>
            }
            
            <div className='align-left margin-bottom-between'>
                <h3>Password</h3>
                <Password 
                    value={password} 
                    placeholder='Password'
                    onChange={(e) => setPassword(e.target.value)} 
                    header={<h6>Pick a password</h6>} 
                    footer={PasswordTemplate} 
                    toggleMask />
            </div>
            <div className='align-center margin-bottom-between'>
                <Button label="Sign Up" className="p-button-raised p-button-info p-button-raised p-button-info bg-white text-blue-400" onClick={() => handleRegister()}/>
            </div>
            <div className='align-center margin-bottom-between'>
                <p className='margin-zero'>Have a account?</p>
                <Link to='/' className='text-white no-underline mt-3'>Log in now</Link>
            </div>
        </div>
        <Toast ref={toast} />
    </div>
  );
}

export default Register;
