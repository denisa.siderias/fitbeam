import React from "react";
import { useContext, useEffect, useState } from 'react';
import { UserContext } from '../contexts/UserContext';
import '../styles/Profile.css';
import { Message } from 'primereact/message';
import { Chart } from 'primereact/chart';
import { getUsersData } from "../services/UserOperations";
import { listenerChangeUsersData } from "../services/ListenerData";

const Statistics = () => {
    const { userData: {user} } = useContext(UserContext);
    const [refetchData, setRefetchData] = useState(true);
    const [metricsData, setMetricsData] = useState([]);
    const labelsDateTime = user?.type === "trainer" && user?.checkedAccount ? Array.from(new Set(metricsData?.map(element => new Date(element?.date?.seconds * 1000 + element?.date?.nanoseconds / 1000000).getFullYear() + "/" + 
    new Date(element?.date?.seconds * 1000 + element?.date?.nanoseconds / 1000000).getMonth() + "/" + new Date(element?.date?.seconds * 1000 + element?.date?.nanoseconds / 1000000).getDate()))) : [];
    
    const viewsPerExercise = user?.type === "trainer" && user?.checkedAccount ? Array.from(new Set((metricsData ?? []).map((element) => element.id_exercise)))
        .map(exercise_id => {
            const dateTimeArr = Array.from(new Set(metricsData?.map(element => new Date(element?.date?.seconds * 1000 + element?.date?.nanoseconds / 1000000).getFullYear() + "/" + 
                    new Date(element?.date?.seconds * 1000 + element?.date?.nanoseconds / 1000000).getMonth() + "/" + new Date(element?.date?.seconds * 1000 + element?.date?.nanoseconds / 1000000).getDate())));

            const dataCal = metricsData?.filter(ex => ex.id_exercise === exercise_id && 
                dateTimeArr
                .includes(new Date(ex?.date?.seconds * 1000 + ex?.date?.nanoseconds / 1000000).getFullYear() + "/" + 
                new Date(ex?.date?.seconds * 1000 + ex?.date?.nanoseconds / 1000000).getMonth() + "/" + new Date(ex?.date?.seconds * 1000 + ex?.date?.nanoseconds / 1000000).getDate()))

            return {
            type: 'bar',
            label: metricsData?.filter((ex => ex.id_exercise === exercise_id))?.[0]?.name ?? "",
            backgroundColor: "#" + Math.random().toString(16).substr(-6),
            data:labelsDateTime
                .map(ex => {
                    const findMoreCal = Array.from(new Set(dataCal.filter(dat => (new Date(dat?.date?.seconds * 1000 + dat?.date?.nanoseconds / 1000000).getFullYear() + "/" + 
                    new Date(dat?.date?.seconds * 1000 + dat?.date?.nanoseconds / 1000000).getMonth() + "/" + new Date(dat?.date?.seconds * 1000 + dat?.date?.nanoseconds / 1000000).getDate())
                    === ex).map(ex => ex.id_user)));
                    if (findMoreCal.length > 0) {
                        return findMoreCal.length;
                    }
                    return 0;
                })
            }

        })
    : [];

    const handleGetTrainingsData = async () => {
        if (user?.checkedAccount) {
            await getUsersData("users").then(res => {
                const auxArray = []
                res?.forEach(element => {
                    const key = user?.firstName?.toLowerCase() + '-' + user?.lastName?.toLowerCase();
                    const findTrainingByTrainer = (element.data()?.exercises ?? []).filter(info =>  info.id_exercise.includes(key));
                    if (element.data().type === "champion" && findTrainingByTrainer.length > 0) {
                        findTrainingByTrainer.map((exercise) => {
                            auxArray.push({
                                id_user: element.id,
                                gender: element.data().gender,
                                date: exercise.date,
                                id_exercise: exercise.id_exercise,
                                name: exercise.name,
                                time: exercise.time,
                                caloriesConsumed: exercise.caloriesConsumed
                            })
                        })
                    }
                });
                setMetricsData(auxArray);
                setRefetchData(false);
            })
        }
    }



    useEffect(() => {
       if (user && refetchData) {
            handleGetTrainingsData();
       }
    }, [metricsData.length, refetchData]);

    useEffect(() => {
        setInterval(() => {
            listenerChangeUsersData("users")
            .then(res => {
                setMetricsData([]);
                setRefetchData(true);
            })
            .catch(error => {
        
            });
        }, 5000 * 60)

    }, []);
   
     return (
       <div className="Settings align-center">
        {!user?.checkedAccount && <Message severity="error" className='my-4' text="Your account isn't checked, please wait until the admin confirm your account" />}
           <div className='grid p-2 m-0'>
               <div className='col-12 md:col-6 lg:col-6 xl:col-6 border-solid border-1 border-300 border-round align-center'>
                   <h3>Views per month</h3>
                   <Chart 
                       type="bar" 
                       width='80%'
                       height='600px'
                       className="align-center"
                       data={{
                           labels: labelsDateTime,
                           datasets: [
                               {
                                   label: 'Views',
                                   data: labelsDateTime
                                       .map(element => {
                                           return parseInt(Array.from(new Set(metricsData
                                           .filter(item => new Date(item?.date?.seconds * 1000 + item?.date?.nanoseconds / 1000000).getFullYear() + "/" + 
                                           new Date(item?.date?.seconds * 1000 + item?.date?.nanoseconds / 1000000).getMonth() + "/" + new Date(item?.date?.seconds * 1000 + item?.date?.nanoseconds / 1000000).getDate() === element)
                                           .map(exercise => exercise.id_exercise && exercise.id_user))).length)
                                       }),
                                   backgroundColor: 'rgba(255, 159, 64, 0.2)',
                                   borderColor: 'rgb(255, 159, 64)',
                                   borderWidth: 1,
                                   
                               }
                           ]
                       }} 
                       options={{
                        maintainAspectRatio: false,
                        aspectRatio: 0.8,
                        plugins: {
                            legend: {
                                labels: {
                                    fontColor: "#000"
                                }
                            }
                        },
                        scales: {
                            x: {
                                ticks: {
                                    color: "#000",
                                    font: {
                                        weight: 500
                                    }
                                },
                                grid: {
                                    color: "#dadada",
                                    drawBorder: false
                                }
                            },
                            y: {
                                ticks: {
                                    color: "#000"
                                },
                                grid: {
                                    color: "#dadada",
                                    drawBorder: false
                                }
                            }
                        }
                       }} 
                   />
               </div>
               <div className='col-12 md:col-6 lg:col-6 xl:col-6 border-solid border-1 border-300 border-round align-center'>
                   <h3>Genders Average</h3>
                   <Chart 
                       type="bar"
                       width='80%'
                       height='600px'
                       className="align-center"
                       data={{
                            labels: labelsDateTime,
                            datasets: [
                                {
                                    label: 'Man',
                                    data: labelsDateTime
                                        .map(element => {
                                            return parseInt(Array.from(new Set(metricsData
                                            .filter(item => new Date(item?.date?.seconds * 1000 + item?.date?.nanoseconds / 1000000).getFullYear() + "/" + 
                                            new Date(item?.date?.seconds * 1000 + item?.date?.nanoseconds / 1000000).getMonth() + "/" + new Date(item?.date?.seconds * 1000 + item?.date?.nanoseconds / 1000000).getDate() === element
                                            && item.gender === "M")
                                            .map(exercise => exercise.id_exercise && exercise.id_user))).length)
                                        }),
                                    backgroundColor: '#145DA0',
                                    
                                },
                                {
                                    label: 'Women',
                                    data: labelsDateTime
                                        .map(element => {
                                            return parseInt(Array.from(new Set(metricsData
                                            .filter(item => new Date(item?.date?.seconds * 1000 + item?.date?.nanoseconds / 1000000).getFullYear() + "/" + 
                                            new Date(item?.date?.seconds * 1000 + item?.date?.nanoseconds / 1000000).getMonth() + "/" + new Date(item?.date?.seconds * 1000 + item?.date?.nanoseconds / 1000000).getDate() === element
                                            && item.gender === "F")
                                            .map(exercise => exercise.id_exercise && exercise.id_user))).length)
                                        }),
                                    backgroundColor: '#FFC0CB',
                                    
                                }
                            ]
                       }} 
                       options={{
                            maintainAspectRatio: false,
                            aspectRatio: 0.8,
                            plugins: {
                                legend: {
                                    labels: {
                                        fontColor: "#000"
                                    }
                                }
                            },
                            scales: {
                                x: {
                                    ticks: {
                                        color: "#000",
                                        font: {
                                            weight: 500
                                        }
                                    },
                                    grid: {
                                        color: "#dadada",
                                        drawBorder: false
                                    }
                                },
                                y: {
                                    ticks: {
                                        color: "#000"
                                    },
                                    grid: {
                                        color: "#dadada",
                                        drawBorder: false
                                    }
                                }
                            }
                       }} 
                   />
               </div>
               <div className='col-12 md:col-6 lg:col-6 xl:col-6 border-solid border-1 border-300 border-round align-center'>
                   <h3>Views per month per exercise</h3>
                   <Chart 
                       type="bar"
                       width='80%'
                       height='600px'
                       className="align-center"
                       data={{
                           labels: labelsDateTime,
                           datasets: viewsPerExercise
                                       
                       }} 
                       options={{
                           maintainAspectRatio: false,
                           aspectRatio: 0.8,
                           plugins: {
                               tooltips: {
                                   mode: 'index',
                                   intersect: false
                               },
                               legend: {
                                   labels: {
                                       color: "#000"
                                   }
                               }
                           },
                           scales: {
                               x: {
                                   stacked: true,
                                   ticks: {
                                       color: "#000"
                                   },
                                   grid: {
                                       color: "#dadada"
                                   }
                               },
                               y: {
                                   stacked: true,
                                   ticks: {
                                       color: "#000"
                                   },
                                   grid: {
                                       color: "#dadada"
                                   }
                               }
                           }
                       }} 
                   />
               </div>
               <div className='col-12 md:col-6 lg:col-6 xl:col-6 border-solid border-1 border-300 border-round align-center'>
                   <h3>Genders Per Month Per Exercise</h3>
                   <Chart 
                       type="bar"
                       width='80%'
                       height='600px'
                       className="align-center"
                       data={{
                            labels: Array.from(new Set(metricsData.map(data => data.name))),
                            datasets: [
                                {
                                    label: 'Man',
                                    data: Array.from(new Set(metricsData.map(data => data.name)))
                                        .map(element => {
                                            return parseInt(Array.from(new Set(metricsData
                                            .filter(item => item.name === element && item.gender === "M")
                                            .map(exercise => exercise.id_exercise))).length)
                                        }),
                                    backgroundColor: '#145DA0',
                                    
                                },
                                {
                                    label: 'Women',
                                    data: Array.from(new Set(metricsData.map(data => data.name)))
                                        .map(element => {
                                            return parseInt(Array.from(new Set(metricsData
                                            .filter(item => item.name === element && item.gender === "F")
                                            .map(exercise => exercise.id_exercise))).length)
                                        }),
                                    backgroundColor: '#FFC0CB',
                                    
                                }
                            ]
                       }} 
                       options={{
                            maintainAspectRatio: false,
                            aspectRatio: 0.8,
                            plugins: {
                                legend: {
                                    labels: {
                                        fontColor: "#000"
                                    }
                                }
                            },
                            scales: {
                                x: {
                                    ticks: {
                                        color: "#000",
                                        font: {
                                            weight: 500
                                        }
                                    },
                                    grid: {
                                        color: "#dadada",
                                        drawBorder: false
                                    }
                                },
                                y: {
                                    ticks: {
                                        color: "#000"
                                    },
                                    grid: {
                                        color: "#dadada",
                                        drawBorder: false
                                    }
                                }
                            }
                       }} 
                   />
               </div>
           </div>
       </div>
    )
}

export default Statistics;