import { collection, doc, onSnapshot } from "firebase/firestore";
import { firestore } from "../configuration/firebase_config";

let listenerData = undefined;

export const listenerChangeTrainingData = (...args) => {
    return new Promise((resolve, reject) => {
        listenerData = onSnapshot(doc(firestore, ...args),
        (doc) => {
                resolve(doc);
        },
        (error) => {
            reject(error);
        });
    })
}


export const listenerChangeTrainingsData = (...args) => {
    return new Promise((resolve, reject) => {
        onSnapshot(collection(firestore, args[0]),
        (doc) => {
                const auxArray = [];
                if (args[1]) {
                    args[1]([]);
                }
                doc.forEach((element) => {
                    auxArray.push({
                        id: element.id,
                        name: element.data().name,
                        description: element.data().description,
                        thumbnail: element.data().thumbnail,
                        muscleCategory: element.data().muscleCategory,
                        trainingCategory: element.data().trainingCategory,
                        rating: element.data().rating,
                        trainer: element.data().trainer,
                        email_trainer: element.data().email_trainer
                    });
                });
                if (args[1]) {
                    args[1](auxArray);
                }
                resolve(auxArray);
        },
        (error) => {
            reject(error);
        });
    })
}

export const listenerChangeUsersData = (...args) => {
    return new Promise((resolve, reject) => {
        onSnapshot(collection(firestore, args[0]),
        (doc) => {
                resolve(doc);
        },
        (error) => {
            reject(error);
        });
    })
}

export const detachListeners = () => {
    if (listenerData) {
        listenerData();
    }
}