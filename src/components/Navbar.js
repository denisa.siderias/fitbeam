
import React, { useContext, useRef } from 'react'; 
import { Menubar } from 'primereact/menubar';
import { Menu } from 'primereact/menu';
import { Avatar } from 'primereact/avatar';
import { classNames } from 'primereact/utils';
import { getAuth, signOut } from 'firebase/auth';
import { RoutingContext } from '../contexts/RoutingContext';
import { UserContext } from '../contexts/UserContext';
import { serverTimestamp } from '@firebase/firestore';
import { setUserData } from '../services/UserOperations';
import LogoApp from "../images/logo-app.png";
import { useWindowSize } from '../constants/Functions';

export default function Navbar(props) {
    const {
        labelAvatar
    } = props;
    const menu = useRef(null);
    const [width, height] = useWindowSize();
    const { history } = useContext(RoutingContext);
    const auth = getAuth();
    const {userData: {user}} = useContext(UserContext);
    const subMenuItem = [
        { label: 'Profile', icon: 'pi pi-fw pi-user', command: () => history('/profile') },
        { separator: true},
        { template: (item, options) => {
            return (
                <button onClick={async (e) => {
                    if (user) {
                        await setUserData("users", user?.email, {
                            ...user,
                            isLogged: false,
                            lastLogged: serverTimestamp()
                        }).then(async res => {
                            if (res) {
                                await signOut(auth)
                                .then(() => {
                                    // Sign-out successful.
                                    history('/');
                                }).catch((error) => {
                                    // An error happened.
                                });
                               
                            } else {
                            }
                        })
                    }

                }} className={classNames(options.className, 'w-full p-link')}>
                    <p>Logout</p>
                </button>
            )
        }},
    ];
    const items = user?.type === "champion" ? [
        {
            label: 'Home', icon: 'pi pi-fw pi-home',
            command: () => history('/home')
        },
        {
            label: 'Favorites', icon: 'pi pi-fw pi-heart-fill',
            command: () => history(`/favorites/${user?.firstName?.toLowerCase() + '-' + user?.lastName?.toLowerCase()}`)
        },
        {
            label: 'Progress', icon: 'pi pi-fw pi-chart-bar',
            command: () => history('/progress')
        },
        {
            label: 'Messenger', icon: 'pi pi-fw pi-comments',
            command: () => history('/chatmessages')
        }
    ] : user?.type === "admin" ? [
        {
            label: 'Home', icon: 'pi pi-fw pi-home',
            command: () => history('/home')
        },
        {
            label: 'Categories', icon: 'pi pi-fw pi-book',
            command: () => history(`/categories`)
        },
        {
            label: 'Users', icon: 'pi pi-fw pi-users',
            command: () => history(`/users`)
        },
    ] : [
        {
            label: 'Home', icon: 'pi pi-fw pi-home',
            command: () => history('/home')
        },
        {
            label: 'Statistics', icon: 'pi pi-fw pi-chart-line',
            command: () => history('/statistics')
        },
        {
            label: 'Messenger', icon: 'pi pi-fw pi-comments',
            command: () => history('/chatmessages')
        }
    ];
    const smallDeviceItems = user?.type === "champion" ?
        [
            {
                label: 'Home', icon: 'pi pi-fw pi-home',
                command: () => history('/home')
            },
            {
                label: 'Favorites', icon: 'pi pi-fw pi-heart-fill',
                command: () => history(`/favorites/${user?.firstName?.toLowerCase() + '-' + user?.lastName?.toLowerCase()}`)
            },
            {
                label: 'Progress', icon: 'pi pi-fw pi-chart-bar',
                command: () => history('/progress')
            },
            {
                label: 'Messenger', icon: 'pi pi-fw pi-comments',
                command: () => history('/chatmessages')
            },
            { label: 'Profile', icon: 'pi pi-fw pi-user', command: () => history('/profile') },
            { label: 'Logout', icon: 'pi pi-fw pi-lock-open', command: async () => {
                if (user) {
                    await setUserData("users", user?.email, {
                        ...user,
                        isLogged: false,
                        lastLogged: serverTimestamp()
                    }).then(async res => {
                        if (res) {
                            await signOut(auth)
                            .then(() => {
                                // Sign-out successful.
                                history('/');
                            }).catch((error) => {
                                // An error happened.
                            });
                           
                        } else {
                        }
                    })
                }
            }}
        ] :
        user?.type === "admin" ? [
            {
                label: 'Home', icon: 'pi pi-fw pi-home',
                command: () => history('/home')
            },
            {
                label: 'Categories', icon: 'pi pi-fw pi-book',
                command: () => history(`/categories`)
            },
        ]
    :
        [
            {
                label: 'Home', icon: 'pi pi-fw pi-home',
                command: () => history('/home')
            },
            {
                label: 'Statistics', icon: 'pi pi-fw pi-chart-line',
                command: () => history('/statistics')
            },
            {
                label: 'Messenger', icon: 'pi pi-fw pi-comments',
                command: () => history('/chatmessages')
            },
            { label: 'Profile', icon: 'pi pi-fw pi-user', command: () => history('/profile') },
            { label: 'Logout', icon: 'pi pi-fw pi-lock-open', command: async () => {
                if (user) {
                    await setUserData("users", user?.email, {
                        ...user,
                        isLogged: false,
                        lastLogged: serverTimestamp()
                    }).then(async res => {
                        if (res) {
                            await signOut(auth)
                            .then(() => {
                                // Sign-out successful.
                                history('/');
                            }).catch((error) => {
                                // An error happened.
                            });
                           
                        } else {
                        }
                    })
                }
            }}
        ];

    const start = <div className='flex align-items-center justify-content-center'><img alt="logo" src={LogoApp} height="30" className="mr-2"></img><h3 className='text-white mr-3'>Fitbeam</h3></div>;
    const end = <>
        <Menu model={subMenuItem} popup ref={menu} />
        <Avatar label={labelAvatar ?? "U"} shape="circle" onClick={(e) => menu.current.toggle(e)} />
    </>

    return (
        <div className="card">
            <Menubar model={width < 960 ? smallDeviceItems : items} className='navbar' start={start} end={width > 960 && end} />
        </div>
    )
}
        