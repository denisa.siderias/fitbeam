import { UserContext } from '../contexts/UserContext';
import React, { useContext } from 'react';
import { Navigate } from 'react-router-dom';
import { ProgressSpinner } from 'primereact/progressspinner';


function ProtectedRoute(props) {
 
  const { userData: { user } } = useContext(UserContext);

    if (user && props)
        if (user?.permissions?.find(element => props?.path
            .substring(0, props?.path.indexOf('/:') > -1 ? props?.path.indexOf('/:') : props?.path.length) 
            === element))
            return props.children
        else
            return <Navigate to='/fourofour' />
    else
        return <div className='w-full h-screen flex justify-content-center align-items-center'>
            <ProgressSpinner />
        </div>
}

export default ProtectedRoute;
