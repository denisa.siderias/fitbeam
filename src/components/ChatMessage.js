import { Avatar } from 'primereact/avatar';
import React, { useContext } from 'react';
import { UserContext } from '../contexts/UserContext';
import '../styles/ChatMessage.css';

const ChatMessage = (props) => {
    const { userData: { user } } = useContext(UserContext);
    const {
        text,
        sender,
    } = props.message;
    
    return (
        <div className={`message ${user?.email === sender ? 'sent' : 'received'}`}>
            <Avatar label={user?.email === sender ? sender[0]?.toUpperCase() : sender[0]?.toUpperCase()} size="large" shape="circle" />
           <p>{text}</p>
        </div>
        
    );
};

export default ChatMessage;