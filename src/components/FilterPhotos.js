import React, { useContext, useRef } from 'react'; 
import { UserContext } from '../contexts/UserContext';
import DropdownTemplate from './DropdownTemplate';
import { InputNumber } from 'primereact/inputnumber';
import { OverlayPanel } from 'primereact/overlaypanel';
import { Button } from 'primereact/button';

export default function FilterPhotos(props) {
    const op = useRef(null);
    const {userData: {user}} = useContext(UserContext);
    const {
        selectedMuscleCategory,
        setSelectedMuscleCategory,
        selectedTrainingCategory,
        setSelectedTrainingCategory,
        muscleCatOptions,
        trainingCatOptions,
        selectedTrainer,
        setSelectedTrainer,
        trainerOptions,
        ratingNumber,
        setRatingNumber,
    } = props;
    const resetFilter = () => {
        if (setSelectedMuscleCategory) {
            setSelectedMuscleCategory(null);
        }
        if (setSelectedTrainingCategory) {
            setSelectedTrainingCategory(null);
        }
        if (setSelectedTrainer) {
            setSelectedTrainer(null);
        }
        if (setRatingNumber) {
            setRatingNumber(0);
        }
    }
    return (
        <div className='Filter'>
            <Button type="button" icon="pi pi-filter-fill" label="Filter" onClick={(e) => op.current.toggle(e)} />
            <OverlayPanel ref={op} className="w-19rem xl:w-20rem">
                <div className="grid">
                    <div className="col-12 md:col-12 lg:col-12 flex flex-column justify-content-center align-items-start">
                        <Button label="Reset Filter" className='filter-btn' onClick={resetFilter} />
                    </div>
                    <div className="col-12 md:col-12 lg:col-12 flex flex-column justify-content-center align-items-start">
                        <p>Muscle Category</p>
                        <DropdownTemplate
                        value={selectedMuscleCategory} 
                        onChange={(e) => setSelectedMuscleCategory ? setSelectedMuscleCategory(e.value) : ''} 
                        options={muscleCatOptions} 
                        optionLabel="name" 
                        placeholder="Select a option" 
                        filter
                        showClear
                        className="w-16rem" 
                        />
                    </div>
                    <div className="col-12 md:col-12 lg:col-12 flex flex-column justify-content-center align-items-start">
                        <p>Training Category</p>
                        <DropdownTemplate
                        value={selectedTrainingCategory} 
                        onChange={(e) => setSelectedTrainingCategory ? setSelectedTrainingCategory(e.value) : ''} 
                        options={trainingCatOptions} 
                        optionLabel="name" 
                        placeholder="Select a option" 
                        filter
                        showClear
                        className="w-16rem" 
                        />
                    </div>
                        {
                        user?.type !== 'trainer' &&
                        <div className="col-12 md:col-12 lg:col-12 flex flex-column justify-content-center align-items-start">
                            <p>Trainer</p>
                            <DropdownTemplate
                            value={selectedTrainer} 
                            onChange={(e) => setSelectedTrainer ? setSelectedTrainer(e.value) : ''} 
                            options={trainerOptions ?? []} 
                            optionLabel="name" 
                            placeholder="Select a option" 
                            filter
                            showClear
                            className="w-16rem" 
                            />
                        </div>
                        }
                    <div className="col-12 md:col-12 lg:col-12 flex flex-column justify-content-center align-items-start">
                        <p>Rating</p>
                        <InputNumber
                        inputId="minmax-buttons" 
                        value={ratingNumber} 
                        onValueChange={(e) => setRatingNumber ? setRatingNumber(e.value) : ''} 
                        mode="decimal" 
                        showButtons 
                        min={0} 
                        max={5}
                        className="w-full"
                        />
                    </div>
                </div>
            </OverlayPanel>
        </div>
    )
}