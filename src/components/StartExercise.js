import { Button } from "primereact/button";
import { Dialog } from "primereact/dialog";
import { Image } from "primereact/image";
import React, { useContext, useEffect, useState } from "react";
import { useInterval } from 'primereact/hooks';
import { UserContext } from "../contexts/UserContext";

const StartExercise = (props) => {
    const {
        open,
        name,
        description,
        image,
        time,
        timeSpent,
        onHide,
        endExercise
    } = props;
    const [intervalState, setIntervalState] = useState(null);
    const [seconds, setSeconds] = useState(0);
    const [minutes, setMinutes] = useState(0);
    const [hours, setHours] = useState(0);
    const {userData: { user }} = useContext(UserContext);
    useEffect(() => {
        setSeconds(parseInt(time?.split(":")?.[2] ?? 0));
        setMinutes(parseInt(time?.split(":")?.[1] ?? 0));
        setHours(parseInt(time?.split(":")?.[0] ?? 0));
    }, [time])
    useInterval(
        () => {
            if (seconds === 59) {
                if (minutes === 59) {
                    setMinutes(0);
                    setSeconds(0);
                    setHours((prevHours) => prevHours + 1);
                } else {
                    setMinutes((prevMinutes) => prevMinutes + 1);
                    setSeconds(0);
                }
            } else {
                setSeconds((prevSeconds) => prevSeconds + 1);
            }
        },
        1000,
        intervalState
    )
    const handleStartExercise = () => {
        setIntervalState(true);
    };
    const handleEndExercise = () => {
        setSeconds(0);
        setMinutes(0);
        setHours(0);
        setTimeout(() => {
            setIntervalState(false);
        }, 500);
        endExercise(seconds, minutes, hours);
        if (onHide)
            onHide();
    };
    return (
        <Dialog
        header={name + " Exercise"} 
        visible={open} 
        onHide={() => onHide ? onHide() : ''}
        style={{ width: '50vw' }} breakpoints={{ '960px': '75vw', '641px': '100vw' }}
        >
            {
                intervalState ?
                    <>
                        <div>
                            <h3>Time track</h3>
                            <p>{hours < 10 ? "0" + hours : hours}:{minutes < 10 ? "0" + minutes : minutes}:{seconds < 10 ? "0" + seconds : seconds}</p>
                        </div>
                    </>
                :
                    <>
                        <div>
                            <h3>Exercise name</h3>
                            <p>{name}</p>
                        </div>
                        <div>
                            <h3>Description Exercise</h3>
                            <p>{description}</p>
                        </div>
                        <div>
                            <h3>Image Exercise</h3>
                            <Image src={image} alt="Image" width="150" />
                        </div>
                        {
                            user?.type === "champion" &&
                            <div>
                                <h3>Time Spent (HH:MM:SS)</h3>
                                <p>{timeSpent}</p>
                            </div>
                        }
                        
                    </>
            }
        {
            user?.type === "champion" &&
            <div>
                <Button 
                    label={intervalState ? "End Exercise" : "Start Exercise"} 
                    icon="pi pi-stopwatch" className="icon-btn" onClick={() => intervalState ? handleEndExercise() : handleStartExercise()} 
                    autoFocus 
                />
            </div>
        }
        
        </Dialog>
    );
};

export default StartExercise;