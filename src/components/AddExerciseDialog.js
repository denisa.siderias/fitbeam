import React, { useContext, useState } from 'react'; 
import { UserContext } from '../contexts/UserContext';
import { InputText } from 'primereact/inputtext';
import { Dialog } from 'primereact/dialog';
import { Button } from 'primereact/button';
import { InputTextarea } from 'primereact/inputtextarea';
import { FileUpload } from 'primereact/fileupload';
import { setTrainingData } from '../services/TrainingOperations';
import { isEmpty, useWindowSize } from '../constants/Functions';
import { InputNumber } from 'primereact/inputnumber';

export default function AddExerciseDialog(props) {
    const {userData: {user}} = useContext(UserContext);
    const [exerciseDialogName, setExerciseDialogName] = useState('');
    const [caloriesDialogName, setCaloriesDialogName] = useState(0);
    const [gifDialog, setGifDialog] = useState(null);
    const [descriptionDialog, setDescriptionDialog] = useState('');
    const [width, height] = useWindowSize();
    const {
        visibleDialog,
        setVisibleDialog,
        trainingId,
        exercises,
        onSuccess,
        onError
    } = props;

    const handleResetAddExerciseDialog = () => {
        if (setVisibleDialog)
            setVisibleDialog(false);
        setTimeout(() => {
          setExerciseDialogName('');
          setCaloriesDialogName(0);
          setGifDialog(null);
          setDescriptionDialog('');
        }, 500)
    };

    const handleUploadExerciseDialog = async () => {
        if (!isEmpty(exerciseDialogName) && caloriesDialogName && gifDialog &&
          !isEmpty(descriptionDialog) && !isEmpty(user?.firstName) && !isEmpty(user?.lastName) &&
          trainingId && exercises?.filter((element) => element.id_exercise === (trainingId + '-' + (exerciseDialogName.includes(" ") ? exerciseDialogName.split(" ").join("-") : exerciseDialogName).toLowerCase())).length === 0) {
          await setTrainingData("trainings", 
          trainingId, {
             exercises: [...exercises, {
              id_exercise: trainingId + '-' + (exerciseDialogName.includes(" ") ? exerciseDialogName.split(" ").join("-") : exerciseDialogName).toLowerCase(),
              calories: +caloriesDialogName,
              description: descriptionDialog,
              name: exerciseDialogName,
              gif: gifDialog
             }]
          }, { merge: true }).then(res => {
              if (res) {
                if (res?.hasOwnProperty("code")) {
                  if (onError)
                    onError(res);
                } else {
                    if (onSuccess)
                      onSuccess(res);
                  handleResetAddExerciseDialog();
                }
                
                  // showSuccess();
              } else {
                    if (onError)
                        onError(res);
                  // showError();
              }
          })
        }
    
    };

    const customBase64Uploader = async (event) => {
        // convert file to base64 encoded
        event?.files?.map(async file => {
          const reader = new FileReader();
          let blob = await fetch(file.objectURL).then((r) => r.blob()); //blob:url
      
          reader.readAsDataURL(blob);
      
          reader.onloadend = function () {
              const base64data = reader.result;
              setGifDialog(base64data);
          };
        })
    };

    const footerContentDialog = (
        <div>
            <Button label="Cancel" icon="pi pi-times" onClick={() => handleResetAddExerciseDialog()} className="p-button-text icon-btn-outlined" />
            <Button label="Upload Training" icon="pi pi-check" className='icon-btn' onClick={() => handleUploadExerciseDialog()} autoFocus />
        </div>
    );

    return (
        <Dialog
        header="Add Exercise" 
        visible={visibleDialog} 
        onHide={() => setVisibleDialog ? setVisibleDialog(false) : ''}
        style={{ width: width < 1250 ? "75vw" : '50vw' }}
        footer={footerContentDialog}
    >
      <div className='grid'>
        <div className='col-12 md:col-6 lg:col-6'>
          <h3>Exercise name*</h3>
          <InputText
            value={exerciseDialogName}
            onChange={(e) => setExerciseDialogName(e.target.value)}
          />
        </div>
        <div className='col-12 md:col-6 lg:col-6'>
          <h3>Calories per minute*</h3>
          <InputNumber
            value={caloriesDialogName} 
            onValueChange={(e) => setCaloriesDialogName(e.value)} 
            mode="decimal" showButtons min={0} max={100} 
          />
        </div>
        <div className='col-12'>
          <h3>Description*</h3>
          <InputTextarea
            value={descriptionDialog} 
            onChange={(e) => setDescriptionDialog(e.target.value)} 
            rows={6}
            className="w-full" 
          />
        </div>
        <div className='col-12'>
          <h3>Upload Exercise Photo*</h3>
          <FileUpload
            name="demo[]" 
            mode="advanced"
            auto
            customUpload="true"
            chooseOptions={{label:"Choose", icon:"pi pi-plus", className:'icon-btn'}}
            // url={'/memories-5d977.appspot.com/'} 
            // onUpload={customBase64Uploader} 
            uploadHandler={(e) =>customBase64Uploader(e)}
            multiple accept="image/*" 
            maxFileSize={50000} 
            emptyTemplate={<p className="m-0">Drag and drop files to here to upload.</p>} 
          />
        </div>
      </div>
    </Dialog>
    )
}