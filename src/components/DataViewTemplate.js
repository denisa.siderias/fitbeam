
import React, { useContext } from 'react';
import { DataView, DataViewLayoutOptions } from 'primereact/dataview';
import { Button } from 'primereact/button';
import { Rating } from 'primereact/rating';
import { RoutingContext } from '../contexts/RoutingContext';
import { Image } from 'primereact/image';
import Muscle from "../images/muscle.png";
import { useWindowSize } from '../constants/Functions';

export default function DataViewTemplate(props) {
    const { history } = useContext(RoutingContext);

    const {
        data,
        layout,
        rows,
        setLayout,
        renderListItem,
        renderGridItem,
        text
    } = props;
    const [width, height] = useWindowSize();

    const renderListItemDefault = (data) => {
        return (
            <div className="col-12">
                <div className="flex flex-column align-items-center p-3 w-full md:flex-row">
                    <Image src={data?.thumbnail} alt="Image" width="250" height="180" />
                    <div className="text-center md:text-left md:flex-1 ml-3">
                        <div className="text-2xl font-bold mb-2">{data?.name}</div>
                        <div className="text-lg mb-2">{data?.trainer}</div>
                        <i className="pi pi-tag vertical-align-middle mr-2"></i>
                        <span className="vertical-align-middle font-semibold">{data?.muscleCategory}</span>
                        <div className="mb-3">{data?.description?.length > 30 ? data?.description?.substring(0, 100) + "..." : data?.description}</div>
                        <Rating className="mb-2" value={data?.rating} readOnly cancel={false}></Rating>
                        <i className="pi pi-tag vertical-align-middle mr-2"></i>
                        <span className="vertical-align-middle font-semibold">{data?.trainingCategory}</span>
                    </div>
                    <div className="flex md:flex-column mt-5 justify-content-between align-items-center md:w-auto w-full">
                        <Button className="mb-2 icon-btn" icon={<img src={Muscle} alt="muscle" width="20" className='mr-2' />} label="View Training" onClick={() => history(`/view-training/${data?.id}`)}></Button>
                    </div>
                </div>
            </div>
        );
    };

    const renderGridItemDefault = (data) => {
        return (
            <div className="col-12 md:col-4">
                <div className="m-2 border-1 border-round-sm p-3 surface-border card">
                    <div className="flex align-items-center justify-content-between mb-4">
                        <div>
                            <i className="pi pi-tag vertical-align-middle mr-2"></i>
                            <span className="font-semibold vertical-align-middle">{data?.muscleCategory}</span>
                        </div>
                        <div>
                            <i className="pi pi-tag vertical-align-middle mr-2"></i>
                            <span className="font-semibold vertical-align-middle">{data?.trainingCategory}</span>
                        </div>
                    </div>
                    <div className="text-center">
                        <Image src={data?.thumbnail} alt="Image" 
                            width={width < 1150 ? width < 1020 ? "200" : "250" :"300"} 
                            height={width < 1150 ? "150" : "200"} />
                        <div className="text-2xl font-bold mt-4">{data?.name}</div>
                        <div className="text-lg mt-2">{data?.trainer}</div>
                        <div className="mb-3">{data?.description?.length > 30 ? data?.description?.substring(0, 100) + "..." : data?.description}</div>
                        <Rating className="mb-3" value={data?.rating ?? 0} readOnly cancel={false}></Rating>
                    </div>
                    <div className="flex align-items-center justify-content-between">
                        <Button icon={<img src={Muscle} alt="muscle" width="20" className='mr-2' />} className="icon-btn" label="View Training" onClick={() => history(`/view-training/${data?.id}`)}></Button>
                    </div>
                </div>
            </div>
        );
    };

    const itemTemplate = (product, layout) => {
        if (!product) {
            return;
        }

        if (layout === 'list') return renderListItem ? renderListItem(product) : renderListItemDefault(product);
        else if (layout === 'grid') return renderGridItem ? renderGridItem(product) : renderGridItemDefault(product);
    };

    const renderHeader = () => {
        return (
            <div className="grid grid-nogutter dataview-header">
                <div className="col-6" style={{ textAlign: 'left' }}>
                    {text && <h3>{text}</h3>}
                </div>
                <div className="col-6" style={{ textAlign: 'right' }}>
                    <DataViewLayoutOptions layout={layout} onChange={(e) => setLayout ? setLayout(e.value) : ''} />
                </div>
            </div>
        );
    };

    const header = renderHeader();

    return (
        <div className="card w-full">
            <DataView value={data} layout={layout} header={header} itemTemplate={itemTemplate} paginator rows={rows} />
        </div>
    )
}