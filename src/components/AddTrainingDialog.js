import React, { useContext, useState } from 'react'; 
import { UserContext } from '../contexts/UserContext';
import DropdownTemplate from './DropdownTemplate';
import { InputText } from 'primereact/inputtext';
import { Dialog } from 'primereact/dialog';
import { Button } from 'primereact/button';
import { InputTextarea } from 'primereact/inputtextarea';
import { FileUpload } from 'primereact/fileupload';
import { setTrainingData } from '../services/TrainingOperations';
import { isEmpty, useWindowSize } from '../constants/Functions';

export default function AddTrainingDialog(props) {
    const {userData: {user}} = useContext(UserContext);
    const [trainingDialogName, setTrainingDialogName] = useState('');
    const [muscleCategoryDialogName, setMuscleCategoryDialogName] = useState(null);
    const [trainingCategoryDialogName, setTrainingCategoryDialogName] = useState(null);
    const [thumbnailDialog, setThumbnailDialog] = useState(null);
    const [descriptionDialog, setDescriptionDialog] = useState('');
    const [width, height] = useWindowSize();
    const {
        visibleDialog,
        setVisibleDialog,
        muscleCategoryOptions,
        trainingCategoryOptions,
        onSuccess,
        onError
    } = props;

    const handleResetAddTrainingDialog = () => {
        if (setVisibleDialog)
            setVisibleDialog(false);
        setTimeout(() => {
          setTrainingDialogName('');
          setMuscleCategoryDialogName(null);
          setTrainingCategoryDialogName(null);
          setDescriptionDialog('');
          setThumbnailDialog(null);
        }, 500)
    };

    const handleUploadTrainingDialog = async () => {
        await setTrainingData("trainings", 
        trainingDialogName.toLowerCase().split(' ').join('-') + '-' + user.firstName.toLowerCase() + '-' + user.lastName.toLowerCase(), {
            muscleCategory: muscleCategoryDialogName.code,
            trainingCategory: trainingCategoryDialogName.code,
            description: descriptionDialog,
            email_trainer: user?.email,
            name: trainingDialogName,
            thumbnail: thumbnailDialog,
            trainer: user?.firstName + ' ' + user?.lastName,
            rating: 0
        }).then(res => {
            if (res) {
              if (onSuccess)
                  onSuccess(res);
              handleResetAddTrainingDialog();
                // showSuccess();
            } else {
                  if (onError)
                      onError(res);
                // showError();
            }
        })
    };

    const customBase64Uploader = async (event) => {
        // convert file to base64 encoded
        event?.files?.map(async file => {
          const reader = new FileReader();
          let blob = await fetch(file.objectURL).then((r) => r.blob()); //blob:url
      
          reader.readAsDataURL(blob);
      
          reader.onloadend = function () {
              const base64data = reader.result;
              setThumbnailDialog(base64data);
          };
        })
    };

    const footerContentDialog = (
        <div>
            <Button label="Cancel" icon="pi pi-times" className="icon-btn-outlined" onClick={() => handleResetAddTrainingDialog()} />
            <Button 
              label="Upload Training"
              icon="pi pi-check"
              className="icon-btn"
              onClick={() => handleUploadTrainingDialog()} 
              autoFocus
              disabled={isEmpty(trainingDialogName) || !muscleCategoryDialogName || !trainingCategoryDialogName ||
                isEmpty(descriptionDialog) || isEmpty(user?.firstName) || isEmpty(user?.lastName)}
            />
        </div>
    );

    const headerTemplate = () => {
      return (
        <div className='surface-200 p-3 border-left-1 border-right-1 border-top-1 border-bottom-none border-round-top border-noround-bottom border-solid border-300'>
          <Button label="Choose" icon="pi pi-plus" className='icon-btn'  />
        </div>
      )
    }

    return (
        <Dialog
        header="Add a training" 
        visible={visibleDialog}
        onHide={() => setVisibleDialog ? setVisibleDialog(false) : ''}
        footer={footerContentDialog}
        style={{ width: width < 1250 ? "75vw" : "50vw"}}
    >
      <div className='grid'>
        <div className='col-12 md:col-6 lg:col-6 xl:col-6 align-left' style={{ justifyContent: 'flex-start'}}>
          <h3>Training name</h3>
          <InputText
            value={trainingDialogName}
            onChange={(e) => setTrainingDialogName(e.target.value)}
          />
        </div>
        <div className='col-12 md:col-6 lg:col-6 xl:col-6 align-left' style={{ justifyContent: 'flex-start'}}>
          <h3>Muscle Category</h3>
          <DropdownTemplate
              value={muscleCategoryDialogName} 
              onChange={(e) => setMuscleCategoryDialogName(e.value)} 
              options={muscleCategoryOptions} 
              optionLabel="name" 
              placeholder="Select a option" 
              filter
              showClear
              className="w-full md:w-14rem" 
          />
        </div>
        <div className='col-12 md:col-6 lg:col-6 xl:col-6 align-left' style={{ justifyContent: 'flex-start'}}>
          <h3>Training Category</h3>
          <DropdownTemplate
              value={trainingCategoryDialogName} 
              onChange={(e) => setTrainingCategoryDialogName(e.value)} 
              options={trainingCategoryOptions} 
              optionLabel="name" 
              placeholder="Select a option" 
              filter
              showClear
              className="w-full md:w-14rem" 
          />
        </div>
        <div className='col-12 md:col-6 lg:col-6 xl:col-6 align-left' style={{ justifyContent: 'flex-start'}}>
          <h3>Description</h3>
          <InputTextarea
            value={descriptionDialog} 
            onChange={(e) => setDescriptionDialog(e.target.value)} 
            rows={5} cols={30} 
          />
        </div>
        <div className='col-12 md:col-6 lg:col-6 xl:col-6 align-left' style={{ justifyContent: 'flex-start'}}>
          <h3>Upload Thumbnail</h3>
          <FileUpload
            name="demo[]"
            mode="advanced"
            auto
            customUpload="true"
            // url={'/memories-5d977.appspot.com/'} 
            // onUpload={customBase64Uploader} 
            chooseOptions={{label:"Choose", icon:"pi pi-plus", className:'icon-btn'}}
            uploadHandler={(e) =>customBase64Uploader(e)}
            multiple={false}
            accept="image/*" 
            maxFileSize={1000000} 
            emptyTemplate={<p className="m-0">Drag and drop files to here to upload.</p>} 
          />
        </div>
      </div>
    </Dialog>
    )
}