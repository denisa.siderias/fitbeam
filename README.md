Adresa repository: https://gitlab.upt.ro/denisa.siderias/fitbeam


Pentru a putea rula aplicația Fitbeam sunt necesari urmatorii pași:

1. Trebuie instalat nodejs-ul și pentru acesta se va accesa site-ul https://nodejs.org/en de unde se va descărca veriunea recomandată.
   După ce este descărcat se vor urma pașii din installer.

2. Se va verifica daca nodejs-ul este instalat corect cu ajutorul următoarelor comenzi:
node --version
npm --version

3. În cazul în care este instalat corect se va începe instalarea managerului de pachete yarn:
npm install --global yarn

4. Se va verifica dacă este instalat corect yarn-ul prin rularea comenzii:
yarn --version

5. Dacă nodejs este instalat corespunzator se va rula urmatoarea comandă pentru a instala toate dependințele din package.json:
yarn install

6. Pentru a rula aplicația vom folosi comanda:
yarn run start

7. Pentru a opri din execuție aplicația vom apăsa ctrl C.
